rootProject.name = "protosdc-converter"

pluginManagement {
    includeBuild("build-logic")

    repositories {
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/60445622/packages/maven")
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(
    "xmlprocessor",
    "kotlin-proto-base-mappers",
    "kotlin-xml-base-mappers",
    "converter"
)