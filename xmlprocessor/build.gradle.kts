plugins {
    id("org.somda.sdc.protosdc_converter.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
}

dependencies {
    implementation(libs.apache.xml.schema.core)
}