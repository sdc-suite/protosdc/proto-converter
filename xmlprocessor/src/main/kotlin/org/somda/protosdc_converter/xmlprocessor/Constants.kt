package org.somda.protosdc_converter.xmlprocessor

import javax.xml.namespace.QName

object Constants {
    const val XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema"
    const val MSG_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/message"
    const val PM_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant"
    const val EXT_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/extension"
    const val XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance"

    fun xsiType() = QName(XSI_NAMESPACE, "type")

    fun xsdQName(type: String) = QName(XSD_NAMESPACE, type)

    fun extQName(type: String) = QName(EXT_NAMESPACE, type)

    fun msgQName(type: String ) = QName(MSG_NAMESPACE, type)
}
