package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.apache.ws.commons.schema.XmlSchemaComplexType
import org.apache.ws.commons.schema.XmlSchemaSimpleType
import org.apache.ws.commons.schema.utils.XmlSchemaObjectBase
import javax.xml.namespace.QName

/**
 * Models an XML Schema dependency tree.
 *
 * @param nodeName Qualified name of the node. There are anonymous particles and attributes where no name is available.
 * @param element The actual XML Schema object tied to this node (can be anything, hence it is the very XML Schema object base type).
 * @param typeParentNode Parent node of this object.
 * @param typeChildNodes Child nodes this node depends on.
 */
class XmlSchemaNode(
    val nodeName: QName?,
    val element: XmlSchemaObjectBase?,
    var typeParentNode: XmlSchemaNode? = null,
    val typeChildNodes: MutableList<XmlSchemaNode> = mutableListOf()
) : Cloneable {
    var parent: XmlSchemaNode? = null
    var children: MutableList<XmlSchemaNode> = mutableListOf()

    fun addChild(node: XmlSchemaNode) {
        children.add(node)
        node.parent = this
    }

    /**
     * Searches for any node given a QName.
     *
     * @param qName The QName of the node to search
     * @return The node that was found in somewhere in the tree that contains this node.
     */
    fun searchQName(qName: QName): XmlSchemaNode? {
        val qNames = searchQNameSequence(qName).distinct().toList()
        return qNames.firstOrNull()
    }

    /**
     * Searches for a node of a complex or simple type.
     *
     * @param qName The QName of the node to search
     * @return The node that was found in somewhere in the tree that contains this node.
     */
    fun searchQNameType(qName: QName): XmlSchemaNode? {
        val qNames = searchQNameSequence(qName).distinct().filter {
            it.element is XmlSchemaComplexType
                    || it.element is XmlSchemaSimpleType
                    || it.nodeName?.let { name -> BuiltinTypes.isBuiltinType(name) } ?: false
        }.toList()
        return qNames.firstOrNull()
    }

    private fun searchQNameSequence(qName: QName): Sequence<XmlSchemaNode> {
        return sequence {
            yieldAll(searchQNameSiblings(qName))
            parent?.let {
                yieldAll(it.searchQNameSequence(qName))
            }
            // we're at the root, search all children
            if (parent == null) {
                yieldAll(searchQNameInChildren(qName))
            }
        }
    }

    private fun searchQNameInChildren(qname: QName, leftOf: XmlSchemaNode? = null): Sequence<XmlSchemaNode> {
        return sequence {
            children.forEach {
                if (it === leftOf) {
                    // only search left of this ApacheNode
                    return@forEach
                }
                it.nodeName?.let { childValue ->
                    if (qname == childValue) {
                        yield(it)
                    }
                }
            }
        }
    }

    private fun searchQNameSiblings(qname: QName): Sequence<XmlSchemaNode> {
        return sequence {
            parent?.let {
                yieldAll(it.searchQNameInChildren(qname, this@XmlSchemaNode))
            }
        }
    }

    public override fun clone(): XmlSchemaNode {
        return clone(parent)
    }

    private fun clone(parent: XmlSchemaNode?): XmlSchemaNode {
        // create the new node first
        val newNode = XmlSchemaNode(nodeName, element)
        newNode.parent = parent

        // then clone all children
        children.forEach {
            newNode.children.add(it.clone(newNode))
        }

        // do not clone type children list, instead reattach children to parents once we clone children
        val newParentTypeRef = typeParentNode?.let { it.nodeName?.let { name -> newNode.searchQName(name) } }
        newParentTypeRef?.let {
            it.typeChildNodes.add(newNode)
            newNode.typeParentNode = it
        }

        return newNode
    }

    // todo is this required? Seems to be unused
    fun traverseInOrder(): Sequence<XmlSchemaNode> {
        return sequence {
            children.forEach {
                yieldAll(it.traverseInOrder())
            }
            yield(this@XmlSchemaNode)
        }
    }

    override fun toString() = nodeName?.toString() ?: ("" + super.toString())

    companion object : Logging
}
