package org.somda.protosdc_converter.xmlprocessor

/**
 * Convenience function to avoid ugly parenthesis.
 *
 * Same as `this as? NodeType.Message`
 */
fun NodeType.asMessage(): NodeType.Message? = this as? NodeType.Message