package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.apache.ws.commons.schema.*
import java.util.UUID
import javax.xml.namespace.QName


enum class OutputLanguage {
    Proto,
    Kotlin,
    ProtoToKotlinMapper,
    KotlinToProtoMapper,
    Rust,
    RustQuickXMLParser,
    RustQuickXMLWriter,
    XmlToKotlinMapper,
    KotlinToXmlMapper
}

/**
 * Represents a node (type, object, ...)
 * @property nodeName name of the node (type, ...) .
 * @property children embedded nodes (embedded types, enums, fields)
 * @property dependencies other nodes this node depends on (parent nodes, types used in parameters etc)
 * @property documentation from the loaded document
 * @property ignore true if for whatever reason the node shall be ignored during code generation
 *                       (e.g. used to omit MustUnderstand attributes from extensions, since those are modelled
 *                       by the custom types enricher as a dedicated attribute to the Extension.Item element)
 */
data class BaseNode(
    var nodeName: String,
    val children: MutableList<BaseNode> = mutableListOf(),
    val documentation: String? = null,
    var languageType: MutableMap<OutputLanguage, BaseLanguageType> = mutableMapOf(),
    val originalXmlNode: XmlInfo? = null,
    val isCustomType: Boolean = false,
    var ignore: Boolean = false
) {
    val nodeId = UUID.randomUUID().toString()

    var nodeType: NodeType? = null

    // clustered types have cyclic dependencies between them, they need to be handled by the language as such, most
    // likely by generating them all together somehow.
    var clusteredTypes: List<BaseNode>? = null
    // if true, this cluster was already handled by the language generator and _must not_ be generated again
    var clusterHandled: MutableMap<OutputLanguage, Boolean> = mutableMapOf()

    companion object: Logging

    /**
     * Validates the correctness of this node, i.e. whether it is complete.
     */
    private fun validate() {
        logger.debug("Validating node $this")
        nodeType ?: throw Exception("nodeType cannot be null in valid BaseNode")
        nodeName.isNotBlank() || throw Exception("nodeName cannot be blank")
        when (nodeType) {
            is NodeType.Root -> {
                logger.debug("This is fine, everything is fine.")
            }
            else -> {
                nodeType?.let {
                    it.parent != null || throw Exception("parent node in nodeType must not be null $nodeName")
                }
            }
        }
        logger.debug("Node is valid. $this")
    }

    /**
     * Validates the correctness of this node and the tree below it.
     *
     * @see validate
     */
    fun validateTree() {
        validate()
        children.forEach { it.validateTree() }
    }

    /**
     * List of all dependencies of this node, including all children.
     */
    val dependencies: List<BaseNode>
        get() {
            validate()
            return children.flatMap { it.dependencies } + nodeType!!.dependencies()
        }

    /**
     * Finds the [NodeType.Message] or [NodeType.BuiltinType] for the given QName.
     *
     * @param qname to search for.
     * @return node which is either the message or builtin type responsible for handling the qname,
     *  null if no type was found
     */
    fun findQName(qname: QName): BaseNode? {

        // message and builtin have QNames
        when (nodeType) {
            is NodeType.Message -> {
                if (qname == (nodeType as NodeType.Message).qname) {
                    return this
                }
            }
            is NodeType.BuiltinType -> {
                if (qname == (nodeType as NodeType.BuiltinType).origin) {
                    return this
                }
            }
            else -> Unit
        }
        children.forEach { child ->
            child.findQName(qname)?.let { return it }
        }
        return null
    }

    fun findParent(node: BaseNode): BaseNode? {
        return traversePreOrder()
            .filter { it.children.contains(node) }
            .firstOrNull()
    }

    /**
     * Sequence traversing the tree in pre-order order.
     *
     * See [Wikipedia](https://en.wikipedia.org/wiki/Tree_traversal#Pre-order,_NLR)
     */
    fun traversePreOrder(): Sequence<BaseNode> {
        return sequence {
            yield(this@BaseNode)
            children.forEach {
                yieldAll(it.traversePreOrder())
            }
        }
    }

    override fun toString(): String {
        return "Node {nodeName: $nodeName, nodeType: $nodeType}"
    }

}

sealed class XmlType {
    data object SimpleType: XmlType()
    data object ComplexType: XmlType()
    data class Element(val type: QName?): XmlType()
    data object Attribute: XmlType()
    data object AttributeGroup: XmlType()
    data class AttributeGroupRef(val refType: QName): XmlType()
    class Restriction(val baseType: QName): XmlType()
    data object Union: XmlType()
    data class List(val itemType: QName): XmlType()
    data class SimpleContentExtension(val baseType: QName): XmlType()
    data class ComplexContentExtension(val baseType: QName): XmlType()
    data object Choice: XmlType()
}

data class XmlInfo(
    val xmlType: XmlType,
    val qName: QName?,
    val typeOnly: Boolean = false,
) {
    companion object: Logging {
        fun fromXmlSchemaNode(node: XmlSchemaNode?): XmlInfo? {
            return when (val element = node?.element) {
                is XmlSchemaSimpleType -> XmlInfo(XmlType.SimpleType, element.qName)
                is XmlSchemaComplexType -> XmlInfo(XmlType.ComplexType,element.qName)
                is XmlSchemaElement -> XmlInfo(XmlType.Element(element.schemaTypeName), element.wireName)
                is XmlSchemaAttribute -> {
                    XmlInfo(XmlType.Attribute, element.wireName)
                }
                is XmlSchemaAttributeGroup -> XmlInfo(XmlType.AttributeGroup, element.qName)
                is XmlSchemaAttributeGroupRef -> XmlInfo(XmlType.AttributeGroupRef(element.targetQName), null)
                is XmlSchemaSimpleTypeRestriction -> XmlInfo(XmlType.Restriction(element.baseTypeName), null)
                is XmlSchemaSimpleTypeUnion -> XmlInfo(XmlType.Union, null)
                is XmlSchemaSimpleTypeList -> XmlInfo(XmlType.List(element.itemTypeName), null)
                is XmlSchemaSimpleContentExtension -> XmlInfo(XmlType.SimpleContentExtension(element.baseTypeName), null)
                is XmlSchemaComplexContentExtension -> XmlInfo(XmlType.ComplexContentExtension(element.baseTypeName), null)
                is XmlSchemaChoice -> XmlInfo(XmlType.Choice, null)
                else -> {
                    if (element != null) {
                        logger.warn { "No XmlType for $element" }
                    } else {
                        logger.debug { "No XmlType for $node" }
                    }
                    null
                }
            }
        }
    }
}

data class Restriction(
    val minLength: Int?,
    val maxLength: Int?,
) {

    fun anyRestrictions() = minLength != null || maxLength != null

}

sealed class NodeType(var parent: BaseNode?) {

    abstract fun dependencies(): List<BaseNode>

    /**
     * Root node type for the type tree.
     */
    object Root: NodeType(null) {
        override fun dependencies(): List<BaseNode> {
            throw Exception("Don't call dependencies on the root node, it literally depends on all the things.")
        }

        override fun toString(): String {
            return "Root"
        }
    }

    /**
     * Represents a new "type", i.e. an object.
     * @property parentNode the parent node
     * @property qname associated with the type. Please try to avoid relying on the QName for anything but overrides,
     * as XML should not be relevant for generating these types.
     * @property wasAttribute whether this message was an attribute in xml
     * @property extensionBaseNode base node of this message, if it was an XsdExtension
     * @property customType marks whether this type was added by the converter to work around issues in xml
     * @property skipGenerating marks whether an episode already contained this message so it must not be written into
     * target files again
     */
    data class Message(
        private var _parentNode: BaseNode?,
        val qname: QName?,
        val wasAttribute: Boolean = false,
        val customType: Boolean = false,
    ) : NodeType(_parentNode) {
        var extensionBaseNode: BaseNode? = null
        var skipGenerating: Boolean = false
        var episodePluginData: Map<String, EpisodeData> = mutableMapOf()

        // a simple name can have no dependencies
        override fun dependencies(): List<BaseNode> = emptyList()

        override fun toString(): String {
            return "Message {qname: $qname}"
        }

        // update parent when parent node is updated
        var parentNode: BaseNode?
            get() = _parentNode
            set(value) {
                parent = value
                _parentNode = value
            }
    }

    /**
     * Represents a field of a type.
     */
    data class Parameter(
        val parentNode: BaseNode?,
        var parameterType: NodeType,
        var optional: Boolean = false,
        var list: Boolean = false,
        var wasAttribute: Boolean = false,
        val inheritance: Boolean = false,
        val restriction: Restriction? = null,
    ) : NodeType(parentNode) {
        override fun dependencies(): List<BaseNode> {
            // either return the parent node of the type we depend on or nothing
            return parameterType.parent?.let { listOf(it) } ?: emptyList()
        }
        override fun toString(): String {
            return "Parameter {parameterType.name: ${parameterType.parent?.nodeName}, optional: $optional, list: $list, wasAttribute: $wasAttribute}"
        }
    }

    /**
     * Represents an enum without explicit values.
     *
     * @param parentNode parent node
     * @param enumValues enum values, will ignore duplicates
     */
    data class StringEnumeration(
        val parentNode: BaseNode?,
        private var enumValues: List<String>
    ) : NodeType(parentNode) {
        override fun dependencies(): List<BaseNode> {
            // enumerations have no dependencies
            return emptyList()
        }

        // duplicate free values
        val values = enumValues.distinct()

        override fun toString(): String {
            return "StringEnumeration {values: $values}"
        }
    }

    /**
     * Represents a selection between multiple [Parameter]s.
     *
     * @param defaultTypeParameter default type for artificial OneOfs introduced through inheritance
     */
    data class OneOf(
        val parentNode: BaseNode?,
        val defaultTypeParameter: BaseNode?,
        val qName: QName? = null
    ) : NodeType(parentNode) {
        override fun dependencies(): List<BaseNode> {
            return emptyList()
        }

        override fun toString(): String {
            return "OneOf { }"
        }
    }

    /**
     * Represents an XML builtin type, such as string.
     */
    data class BuiltinType(var parentNode: BaseNode?, val origin: QName) : NodeType(parentNode) {
        override fun dependencies(): List<BaseNode> {
            // builtin types have no dependencies
            return emptyList()
        }

        override fun toString(): String {
            return "BuiltinType {origin: $origin}"
        }
    }
}

/**
 * Programming language-specific base type, e.g. that represents Kotlin or Rust.
 */
interface BaseLanguageType