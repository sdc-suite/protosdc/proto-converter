package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.apache.ws.commons.schema.XmlSchemaAll
import org.apache.ws.commons.schema.XmlSchemaAny
import org.apache.ws.commons.schema.XmlSchemaAttribute
import org.apache.ws.commons.schema.XmlSchemaAttributeGroup
import org.apache.ws.commons.schema.XmlSchemaAttributeGroupMember
import org.apache.ws.commons.schema.XmlSchemaAttributeGroupRef
import org.apache.ws.commons.schema.XmlSchemaChoice
import org.apache.ws.commons.schema.XmlSchemaComplexContent
import org.apache.ws.commons.schema.XmlSchemaComplexContentExtension
import org.apache.ws.commons.schema.XmlSchemaComplexType
import org.apache.ws.commons.schema.XmlSchemaContent
import org.apache.ws.commons.schema.XmlSchemaElement
import org.apache.ws.commons.schema.XmlSchemaEnumerationFacet
import org.apache.ws.commons.schema.XmlSchemaFacet
import org.apache.ws.commons.schema.XmlSchemaMaxLengthFacet
import org.apache.ws.commons.schema.XmlSchemaMinLengthFacet
import org.apache.ws.commons.schema.XmlSchemaObject
import org.apache.ws.commons.schema.XmlSchemaSequence
import org.apache.ws.commons.schema.XmlSchemaSimpleContent
import org.apache.ws.commons.schema.XmlSchemaSimpleContentExtension
import org.apache.ws.commons.schema.XmlSchemaSimpleContentRestriction
import org.apache.ws.commons.schema.XmlSchemaSimpleType
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeList
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeRestriction
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeUnion
import org.apache.ws.commons.schema.XmlSchemaUse
import javax.xml.namespace.QName

/**
 * Converts an [XmlSchemaNode] dependency tree into a ProtocolBuffers-compatible representation that is independent of
 * XML Schema artifacts.
 *
 * The resulting tree consists of messages, parameters to Messages, string enumerations, one-ofs and builtin types.
 * Since ProtocolBuffers does not implement inheritance, [BaseTypeProcessor] converts inheritance to composition by
 * adding base classes as parameters to types that extend other types.
 *
 * @param tree The dependency tree to accees XML Schema information.
 * @param attributeSuffix A suffix appended to attributes to avoid name conflicts.
 */
class BaseTypeProcessor(
    private val tree: XmlSchemaNode,
    private val attributeSuffix: String
) {
    /** map used to remember which [BaseNode] was created for which [XmlSchemaNode], see [createBaseNode] */
    private val nodeMap: MutableMap<XmlSchemaNode, BaseNode> = mutableMapOf()

    init {
        // create builtin nodes for base
        BuiltinTypes.entries.forEach {
            val node = createBaseNode(tree.searchQNameType(it.qname)!!, it.qname.localPart)
            node.nodeType = NodeType.BuiltinType(node, it.qname)
        }
    }

    /**
     * Generates a tree of [BaseNode] with a root, fully removing any inheritance in favor of composition.
     *
     * @return root node in which children are the types
     */
    fun generate(): BaseNode {
        val nodes = mutableListOf<BaseNode>()

        // add builtin type nodes, which have been created in init
        nodes.addAll(nodeMap.values.toList())

        logger.info { "Start processing..." }
        // add all nodes below root
        nodes.addAll(generateRootTypes())
        logger.info { "Processing to base nodes complete. Resolving inheritance..." }

        val rootNode = createBaseNode(tree, "Converter: RootNode", NodeType.Root, nodes, register = false)

        val inheritanceMap = buildInheritanceMap(rootNode)
        attachInheritanceMap(rootNode = rootNode, inheritanceMap = inheritanceMap)

        // reorder the tree to represent the new hierarchy required
        val (rootChildNodes, resolvedNodes) = resolveNodes(
            iterations = RESOLUTION_ITERATIONS,
            childNodes = rootNode.children.toMutableList(),
            resolved = mutableListOf()
        )

        val (rootChildNodesNoCluster, resolvedNodesNoCluster) = resolveClusters(rootChildNodes, resolvedNodes)

        if (rootChildNodesNoCluster.isNotEmpty()) {
            throw Exception("Could not reorder tree, remaining elements: $rootChildNodesNoCluster")
        }

        rootNode.children.clear()
        rootNode.children.addAll(resolvedNodesNoCluster)

        logger.info("Final order of children: ${rootNode.children.joinToString { it.nodeName }}")

        return rootNode
    }

    /**
     * Creates a new [BaseNode].
     *
     * @param xmlNode to create node for
     * @param nodeName name of the new node
     * @param nodeType type of the new node, can be set later on
     * @param children of the new node, can be added later on
     * @param documentation of the node TODO LDe: Currently unused
     * @param register if true, this node will be registered as the node responsible for the [xmlNode] passed.
     *  Only register messages, not parameters, they're not the responsible nodes for a type!
     */
    private fun createBaseNode(
        xmlNode: XmlSchemaNode?,
        nodeName: String,
        nodeType: NodeType? = null,
        children: MutableList<BaseNode> = mutableListOf(),
        documentation: String? = null,
        register: Boolean = true
    ): BaseNode {
        val baseNode = BaseNode(nodeName, children, documentation, originalXmlNode = XmlInfo.fromXmlSchemaNode(xmlNode))
        baseNode.nodeType = nodeType
        if (register) {
            xmlNode?.let {
                nodeMap[xmlNode] = baseNode
            }
        }
        return baseNode
    }

    /**
     * Parses an xsd:simpleType's parameter, used in multiple branches of [generateSimpleType].
     */
    private fun resolveXmlSchemaSimpleTypeListParameter(
        simpleType: XmlSchemaSimpleTypeList,
        child: XmlSchemaNode
    ): Pair<BaseNode, NodeType.Parameter> {
        val itemTypeNode = child.searchQNameType(simpleType.itemTypeName)
        checkNotNull(itemTypeNode) { "${simpleType.itemTypeName} needs to be known by $child, but was not" }

        val itemTypeBaseNode = nodeMap[itemTypeNode]!!
        val itemTypeBaseNodeType = itemTypeBaseNode.nodeType!!

        val member = createBaseNode(child, itemTypeBaseNode.nodeName)
        val parameter = NodeType.Parameter(member, itemTypeBaseNodeType, list = true)
        member.nodeType = parameter

        return Pair(member, parameter)
    }

    private fun resolveSimpleTypeParameter(
        itemTypeQName: QName,
        child: XmlSchemaNode
    ): Pair<BaseNode, NodeType.Parameter> {
        val itemTypeNode = child.searchQNameType(itemTypeQName)
        checkNotNull(itemTypeNode)
        val itemTypeBaseNode = nodeMap[itemTypeNode]!!
        val itemTypeBaseNodeType = itemTypeBaseNode.nodeType!!

        val member = createBaseNode(child, itemTypeBaseNode.nodeName)
        val parameter = NodeType.Parameter(member, itemTypeBaseNodeType, list = false)
        member.nodeType = parameter

        return Pair(member, parameter)
    }

    /**
     * Parses an xsd:simpleType node.
     */
    private fun generateSimpleType(
        xmlNode: XmlSchemaNode,
        typeName: String? = null
    ): ReturnType {
        val parameters: List<BaseNode> = xmlNode.children.map { child ->
            when (val element = child.element) {
                is XmlSchemaSimpleTypeRestriction -> {
                    when (val restriction = generateRestriction(child, element)) {
                        is ReturnType.Message -> {
                            // create a parameter for the new restriction type
                            val parameterNode = createBaseNode(child, restriction.node.nodeName, register = false)
                            val parameter = NodeType.Parameter(parameterNode, restriction.node.nodeType!!)
                            parameterNode.nodeType = parameter

                            listOf(restriction.node, parameterNode)
                        }

                        is ReturnType.Parameters -> {
                            restriction.nodes
                        }

                        else -> {
                            throw Exception("Cannot handle parameters here")
                        }
                    }
                }

                is XmlSchemaSimpleTypeList -> {
                    val parameter = resolveXmlSchemaSimpleTypeListParameter(element, child)
                    listOf(parameter.first)
                }

                is XmlSchemaSimpleTypeUnion -> {
                    val unionNodeTypes = element.memberTypesQNames.map {
                        resolveSimpleTypeParameter(it, child).first
                    }.toList()

                    // this might be the one case where looking up the parent name is ok
                    val nodeName = typeName
                        ?: xmlNode.nodeName?.localPart
                        ?: xmlNode.parent?.nodeName?.localPart
                        ?: run {
                            throw Exception("Cannot determine union node name for $xmlNode")
                        }

                    val unionBaseNode = createBaseNode(child, nodeName)
                    val union = NodeType.OneOf(unionBaseNode, defaultTypeParameter = null, xmlNode.nodeName)
                    unionBaseNode.nodeType = union

                    unionBaseNode.children.addAll(unionNodeTypes)

                    listOf(unionBaseNode)
                }

                else -> throw Exception("Unsupported type: ${child.element} (${child.element?.javaClass ?: ""})")
            }
        }.flatten()

        // a simple type without name is anonymous and not a message, but instead a collection of parameters

        when (xmlNode.nodeName) {
            null -> return ReturnType.Parameters(parameters)
            else -> {
                val actualTypeName = typeName
                    ?: xmlNode.nodeName.localPart
                    ?: run {
                        throw Exception("Could not determine type name for complex type $xmlNode")
                    }

                val baseNodeType = NodeType.Message(null, xmlNode.nodeName)
                val baseNode = createBaseNode(xmlNode, actualTypeName, baseNodeType, mutableListOf())
                baseNodeType.parentNode = baseNode

                baseNode.children.addAll(parameters)

                return ReturnType.Message(baseNode)
            }
        }
    }

    /**
     * Parses an xsd:complexType node.
     */
    private fun generateComplexType(xmlNode: XmlSchemaNode): ReturnType {
        val potentialTypeName = xmlNode.nodeName?.localPart ?: "anonymous complex type"

        var baseType: BaseNode? = null

        // choices need access to their parent, that's why we have to create the parent baseNode prior to
        // processing the children
        val baseNode = when (xmlNode.nodeName != null) {
            true -> {
                val actualTypeName = xmlNode.nodeName.localPart
                    ?: run {
                        logger.warn("Could not determine type name for complex type $xmlNode")
                        "FIXME_ANONYMOUS_COMPLEX_TYPE"
                    }

                val baseNodeType = NodeType.Message(null, xmlNode.nodeName)
                baseNodeType.extensionBaseNode = baseType
                val localBaseNode = createBaseNode(xmlNode, actualTypeName, baseNodeType, mutableListOf())
                baseNodeType.parentNode = localBaseNode
                localBaseNode
            }

            false -> null
        }

        val parameters: List<BaseNode> = xmlNode.children.map { child ->
            when (child.element) {
                is XmlSchemaSimpleContent -> {
                    logger.debug("Parsing XmlSchemaSimpleContent in $potentialTypeName")
                    when (val simpleContent = generateSimpleContent(child)) {
                        is ReturnType.Message -> {
                            listOf(simpleContent.node)
                        }

                        is ReturnType.Parameters -> {
                            simpleContent.nodes
                        }

                        is ReturnType.Extension -> {
                            // register this type as an extension of the base type
                            logger.info("Registering $potentialTypeName as extension of ${simpleContent.baseNode.nodeName}")
                            baseType = simpleContent.baseNode
                            simpleContent.parameters.nodes
                        }
                    }
                }

                is XmlSchemaComplexContent -> {
                    logger.debug("Parsing XmlSchemaComplexContent in $potentialTypeName")
                    when (val complexContent = generateComplexContent(child)) {
                        is ReturnType.Message -> {
                            listOf(complexContent.node)
                        }

                        is ReturnType.Parameters -> {
                            complexContent.nodes
                        }

                        is ReturnType.Extension -> {
                            // register this type as an extension of the base type
                            logger.info("Registering $potentialTypeName as extension of ${complexContent.baseNode.nodeName}")
                            baseType = complexContent.baseNode
                            complexContent.parameters.nodes
                        }
                    }
                }

                is XmlSchemaSequence -> {
                    logger.debug("Parsing XmlSchemaSequence in $potentialTypeName")
                    generateSequence(child)
                }

                is XmlSchemaChoice -> {
                    logger.debug("Parsing XmlSchemaChoice in $potentialTypeName")
                    generateChoice(child)
                }

                is XmlSchemaAttribute -> {
                    logger.debug("Parsing XmlSchemaAttribute in $potentialTypeName")
                    when (val result = generateAttribute(child, child.element)) {
                        is ReturnType.Message -> listOf(result.node)
                        is ReturnType.Parameters -> result.nodes
                        else -> throw Exception("Unknown type $result")
                    }
                }

                is XmlSchemaAttributeGroup -> {
                    logger.debug("Parsing XmlSchemaAttributeGroup in $potentialTypeName")
                    listOf(generateAttributeGroup(child, child.element))
                }

                is XmlSchemaAttributeGroupRef -> {
                    logger.debug("Parsing XmlSchemaAttributeGroupRef in $potentialTypeName")
                    listOf(generateAttributeGroup(child, child.element))
                }

                else -> throw Exception("Unsupported type: ${child.element} (${child.element?.javaClass ?: ""})")
            }
        }.flatten()

        // a complex type without name is anonymous and not a message, but instead a collection of parameters
        when (xmlNode.nodeName != null) {
            true -> {
                baseNode!!.children.addAll(parameters)
                (baseNode.nodeType as NodeType.Message).extensionBaseNode = baseType
                return ReturnType.Message(baseNode)
            }

            false -> {
                val returnTypeParameters = ReturnType.Parameters(parameters)
                return baseType?.let {
                    ReturnType.Extension(returnTypeParameters, it)
                } ?: returnTypeParameters
            }
        }
    }

    /**
     * Parses an xsd:simpleContent node.
     */
    private fun generateSimpleContent(xmlNode: XmlSchemaNode): ReturnType {
        return xmlNode.children.first().let { child ->
            when (child.element) {
                is XmlSchemaSimpleContentExtension -> generateExtension(child, child.element)
                is XmlSchemaSimpleContentRestriction -> generateRestriction(child, child.element)
                else -> throw Exception("No handler in generateSimpleContent for ${child.element!!::class.java}")
            }
        }
    }

    /**
     * Parses an xsd:complexContent node.
     */
    private fun generateComplexContent(xmlNode: XmlSchemaNode): ReturnType {
        xmlNode.children.forEach { child ->
            when (child.element) {
                is XmlSchemaComplexContentExtension -> {
                    return generateExtension(child, child.element)
                }

                else -> {
                    throw Exception("Handle generateComplexContent ${child.element}")
                }
            }
        }
        throw Exception("Handle falling through cases in generateComplexContent")
    }

    /**
     * Parses an xsd:attribute node.
     */
    private fun generateAttribute(xmlNode: XmlSchemaNode, attribute: XmlSchemaAttribute): ReturnType {
        check(attribute.name != null || attribute.isRef) { "attribute name and ref is null, makes no sense" }
        val attributeRequired = attribute.use == XmlSchemaUse.REQUIRED

        // attribute is defined here
        val targetAttribute = when (attribute.isRef) {
            true -> attribute.ref.target
            false -> attribute
        }

        targetAttribute.schemaTypeName?.let { qname ->
            val xmlTypeNode = xmlNode.searchQNameType(qname)
            checkNotNull(xmlTypeNode)
            val typeParentNode = nodeMap[xmlTypeNode]!!
            val typeNode = typeParentNode.nodeType!!

            // if we're first level, generate a message, parameter otherwise
            xmlNode.parent?.element?.let {
                logger.info("${attribute.name} is embedded attribute, generating parameter")
                val baseNode = createBaseNode(xmlNode, attribute.name ?: attribute.ref!!.targetQName!!.localPart)
                val parameter =
                    NodeType.Parameter(baseNode, typeNode, wasAttribute = true, optional = !attributeRequired)
                baseNode.nodeType = parameter
                return ReturnType.Parameters(listOf(baseNode))
            } ?: run {
                logger.info("${attribute.name} is root level attribute, generating type with parameter")
                val baseNode = createBaseNode(xmlNode, attribute.name ?: qname.localPart)
                val message = NodeType.Message(baseNode, xmlNode.nodeName)
                baseNode.nodeType = message

                val parameterNode = createBaseNode(xmlNode, attribute.name, register = false)
                val parameter = NodeType.Parameter(parameterNode, typeNode, optional = !attributeRequired)
                parameterNode.nodeType = parameter
                baseNode.children.add(parameterNode)

                return ReturnType.Message(baseNode)
            }
        }

        // attributes may define new types for them to use as attributes
        xmlNode.children.forEach { child ->
            when (child.element) {
                is XmlSchemaSimpleType -> {
                    when (val simpleType = generateSimpleType(child)) {
                        is ReturnType.Message -> {
                            throw Exception("Cannot handle non anonymous complex type embedded in attribute")
                        }

                        is ReturnType.Parameters -> {
                            // to avoid naming clashes, append a configured suffix to types which are from attributes.
                            // this is somewhat hacky, and _can_ fail, we just don't care right now
                            // TODO LDe: Handle clashing names between attributes and fields in a more refined manner
                            val baseNode = createBaseNode(xmlNode, "${attribute.name}$attributeSuffix")
                            val message = NodeType.Message(baseNode, xmlNode.nodeName)
                            baseNode.nodeType = message
                            baseNode.children.addAll(simpleType.nodes)

                            val parameterNode = createBaseNode(xmlNode, attribute.name, register = false)
                            val parameter = NodeType.Parameter(
                                parameterNode,
                                message,
                                wasAttribute = true,
                                optional = !attributeRequired
                            )
                            parameterNode.nodeType = parameter

                            return ReturnType.Parameters(listOf(baseNode, parameterNode))

                        }

                        else -> Unit
                    }
                    // TODO LDe: If we want to remove some indirections, this would have to handle restrictions as well
                }

                else -> throw Exception("Unsupported type: ${child.element} (${child.element?.javaClass})")
            }
        }

        throw Exception("Handle falling through cases in generateAttribute")
    }

    /**
     * Resolves a ref attribute on an element.
     *
     * @return Pair in which the key is the node of the referenced type, value the [NodeType].
     */
    private fun resolvedRefTag(
        targetQName: QName,
        xmlNode: XmlSchemaNode
    ): Pair<BaseNode, NodeType> {
        // TODO LDe: Should filter for only attribute group types
        val xmlTypeNode = xmlNode.searchQName(targetQName)
        checkNotNull(xmlTypeNode)
        val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
        val typeNode = typeParentNode.nodeType!!

        return Pair(typeParentNode, typeNode)
    }

    /**
     * Parses an xsd:attributeGroup node.
     */
    private fun generateAttributeGroup(
        xmlNode: XmlSchemaNode,
        attributeGroup: XmlSchemaAttributeGroupMember
    ): BaseNode {
        when (attributeGroup) {
            is XmlSchemaAttributeGroupRef -> {
                val (typeParentNode, typeNode) = resolvedRefTag(attributeGroup.targetQName, xmlNode)
                val baseNode = createBaseNode(xmlNode, attributeGroup.ref.target.name ?: typeParentNode.nodeName)
                val parameter = NodeType.Parameter(baseNode, typeNode, wasAttribute = true)
                baseNode.nodeType = parameter
                return baseNode
            }

            is XmlSchemaAttributeGroup -> {
                val nodes = xmlNode.children.map {
                    when (it.element) {
                        is XmlSchemaAttribute -> generateAttribute(it, it.element)
                        else -> throw Exception("Only attributes are allowed as children in attributeGroup")
                    }
                }.map {
                    when (it) {
                        is ReturnType.Message -> listOf(it.node)
                        is ReturnType.Parameters -> it.nodes
                        else -> throw Exception("Unknown return type: $it")
                    }
                }.flatten()

                val baseNode = createBaseNode(xmlNode, attributeGroup.name)
                val type = NodeType.Message(baseNode, xmlNode.nodeName)
                baseNode.nodeType = type
                baseNode.children.addAll(nodes)
                return baseNode
            }
        }

        throw Exception("Unsupported attribute group: $attributeGroup (${attributeGroup.javaClass})")
    }

    /**
     * Parses an xsd:extension node.
     */
    private fun generateExtension(xmlNode: XmlSchemaNode, extension: XmlSchemaContent): ReturnType {
        val baseTypeQName = when (extension) {
            is XmlSchemaSimpleContentExtension -> extension.baseTypeName
            is XmlSchemaComplexContentExtension -> extension.baseTypeName
            else -> throw Exception("Unknown extension type: ${extension::class.java}")
        }

        // find referenced base type node
        val baseXmlNode = xmlNode.searchQNameType(baseTypeQName)!!
        val baseNode = nodeMap[baseXmlNode]!!

        // create related node
        val extensionNode = createBaseNode(xmlNode, baseNode.nodeName)
        val extensionNodeType = NodeType.Parameter(
            extensionNode,
            baseNode.nodeType!!,
            inheritance = extension is XmlSchemaComplexContentExtension
        )
        extensionNode.nodeType = extensionNodeType

        // add child for referenced base type
        val extensionParameters = xmlNode.children.map { child ->
            when (child.element) {
                is XmlSchemaSequence -> generateSequence(child)
                is XmlSchemaAttribute -> when (val result = generateAttribute(child, child.element)) {
                    is ReturnType.Message -> listOf(result.node)
                    is ReturnType.Parameters -> result.nodes
                    else -> throw Exception("Unknown return type $result")
                }

                is XmlSchemaAttributeGroupMember -> {
                    listOf(generateAttributeGroup(child, child.element))
                }

                is XmlSchemaChoice -> throw Exception("XML Schema Choice is not supported in extensions (found at ${child.element})")

                is XmlSchemaAll -> throw Exception("No support for ${child.element.javaClass}")

                else -> throw Exception("Handle ${child.element} failed due to unexpected particle: ${child.element?.javaClass}")
            }
        }.flatten()

        val returnParameters = ReturnType.Parameters(listOf(extensionNode) + extensionParameters)
        return when (extension) {
            is XmlSchemaSimpleContentExtension -> returnParameters
            is XmlSchemaComplexContentExtension -> ReturnType.Extension(
                parameters = returnParameters,
                baseNode = baseNode
            )

            else -> throw Exception("Unknown extension type: ${extension::class.java}")
        }
    }

    /**
     * Parses an xsd:element node.
     */
    private fun generateElement(
        xmlNode: XmlSchemaNode,
        element: XmlSchemaElement,
        isRootLevel: Boolean = false
    ): BaseNode {
        element.id?.let {
            throw Exception("Cannot handle id in generateElement for $element")
        }

        when (element.isRef) {
            true -> {
                val (_, typeNode) = resolvedRefTag(element.targetQName, xmlNode)
                val elementName = element.name ?: run {
                    val newName = "${typeNode.parent!!.nodeName}Element"
                    logger.warn("Could not extract element name, setting to $newName")
                    newName
                }
                val baseNode = createBaseNode(xmlNode, elementName)
                val parameter =
                    NodeType.Parameter(baseNode, typeNode, optional = isOptional(element), list = isList(element))
                baseNode.nodeType = parameter
                return baseNode
            }

            false -> element.schemaTypeName?.let {
                val xmlTypeNode = xmlNode.searchQNameType(element.schemaTypeName)
                checkNotNull(xmlTypeNode)
                val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
                val typeNode = typeParentNode.nodeType!!

                val baseNode = createBaseNode(xmlNode, element.name)

                when (isRootLevel) {
                    true -> {
                        logger.info("Creating message parent node for root element type ${element.name}")
                        // add a message node to wrap the parameter in
                        val message = NodeType.Message(baseNode, qname = xmlNode.nodeName)
                        baseNode.nodeType = message

                        val typeName = xmlNode.nodeName?.localPart!!
                        val parameterNode = createBaseNode(xmlNode, typeName, register = false)
                        val parameter = NodeType.Parameter(
                            parameterNode, typeNode,
                            list = isList(element), optional = isOptional(element)
                        )
                        parameterNode.nodeType = parameter

                        baseNode.children.add(parameterNode)

                        return baseNode
                    }

                    false -> {
                        val parameter = NodeType.Parameter(
                            baseNode, typeNode,
                            list = isList(element), optional = isOptional(element)
                        )
                        baseNode.nodeType = parameter
                        return baseNode
                    }
                }
            }
        }

        // case no ref and no schema type name available:

        val baseNode = createBaseNode(xmlNode, element.name)
        val message = NodeType.Message(baseNode, xmlNode.nodeName)
        baseNode.nodeType = message

        xmlNode.children.forEach { child ->
            when (child.element) {
                is XmlSchemaComplexType -> {
                    when (val returnType = generateComplexType(child)) {
                        is ReturnType.Extension -> {
                            baseNode.children.addAll(returnType.parameters.nodes)
                            message.extensionBaseNode = returnType.baseNode
                        }

                        is ReturnType.Parameters -> {
                            baseNode.children.addAll(returnType.nodes)
                        }

                        is ReturnType.Message -> {
                            throw Exception("Cannot process not anonymous complex type as child of element")
                        }
                    }
                }

                is XmlSchemaSimpleType -> {
                    when (val returnType = generateSimpleType(child)) {
                        is ReturnType.Parameters -> {
                            baseNode.children.addAll(returnType.nodes)
                        }

                        is ReturnType.Message -> {
                            throw Exception("Cannot process not anonymous simple type as child of element")
                        }

                        else -> Unit
                    }
                }

                is XmlSchemaElement -> {
                    baseNode.children.add(generateElement(child, child.element))
                }

                else -> throw Exception("No support for: ${child.element} (${child.element?.javaClass})")
            }
        }

        return baseNode
    }

    /**
     * Parses an xsd:choice node
     *
     * - xsd:choice is currently supported only below complex type and element complex content definitions
     * - usage in extensions/restrictions or nested choices are out of consideration.
     */
    private fun generateChoice(xmlNode: XmlSchemaNode): List<BaseNode> {

        val parentNode = when (xmlNode.parent!!.nodeName) {
            null -> {
                checkNotNull(xmlNode.parent!!.parent) {
                    "Choice parent's parent was expected to have a parent since the choice's parent's node name is null"
                }
            }

            else -> xmlNode.parent!!
        }

        val parent = nodeMap[parentNode]

        // create message holding OneOfs
        val oneOfNode = createBaseNode(xmlNode, "choice", null, register = false)
        // note: qName = null
        val oneOf = NodeType.OneOf(parent, defaultTypeParameter = null, qName = null)
        oneOfNode.nodeType = oneOf

        xmlNode.children.forEach { child ->
            when (child.element) {
                is XmlSchemaSequence -> throw Exception("XML Schema anonymous sequence is not supported, but detected here: ${child.element} (${child.element.javaClass})")

                is XmlSchemaAny -> throw Exception("XML Schema any in choice does not make sense: ${child.element} (${child.element.javaClass})")

                is XmlSchemaElement -> oneOfNode.children.add(generateElement(child, child.element))

                else -> throw Exception("No support for: ${child.element} (${child.element?.javaClass})")
            }
        }

        return listOf(oneOfNode)
    }

    /**
     * Parses an xsd:sequence node.
     */
    private fun generateSequence(xmlNode: XmlSchemaNode): List<BaseNode> {

        val nodes = mutableListOf<BaseNode>()

        xmlNode.children.forEach { child ->
            when (child.element) {
                is XmlSchemaSequence -> {
                    // first of all, I hate you for having a sequence in a sequence. I really just want you to know that
                    // matter of fact, I'm gonna log this as a warning for you
                    logger.warn { "A sequence within a sequence is stupid." }
                    nodes.addAll(generateSequence(child))
                }

                is XmlSchemaAny -> Unit // extensions are handled differently

                is XmlSchemaElement -> {
                    val element = generateElement(child, child.element)
                    nodes.add(element)

                    when (val elementType = element.nodeType) {
                        is NodeType.Message -> {
                            // it looks like (aka maybe) generateElement never returns NodeType.Message except when on root level
                            // on root level, however, there is no sequence to generate
                            // todo investigate if when branch can be visited at all

                            // add a parameter for this node, since we've gotten an embedded message
                            logger.debug { "Generating parameter for element $element" }

                            val baseNode = createBaseNode(child, element.nodeName)
                            val parameter = NodeType.Parameter(
                                baseNode,
                                elementType,
                                list = isList(child.element),
                                optional = isOptional(child.element)
                            )
                            baseNode.nodeType = parameter

                            nodes.add(baseNode)
                        }

                        else -> Unit
                    }
                }

                else -> throw Exception("No support for: ${child.element} (${child.element?.javaClass})")
            }
        }

        return nodes
    }

    private fun extractRestrictions(facets: List<XmlSchemaFacet>): Restriction? {
        val parsedRestriction = Restriction(
            minLength = (facets.find { it is XmlSchemaMinLengthFacet }?.value as String?)?.toInt(),
            maxLength = (facets.find { it is XmlSchemaMaxLengthFacet }?.value as String?)?.toInt()
        )

        return when (parsedRestriction.anyRestrictions()) {
            true -> parsedRestriction
            false -> null
        }
    }

    /**
     * Parses an xsd:restriction node.
     */
    private fun generateRestriction(xmlNode: XmlSchemaNode, restriction: XmlSchemaObject): ReturnType {
        val (facets, baseTypeName, _) = when (restriction) {
            is XmlSchemaSimpleContentRestriction -> Triple(
                restriction.facets,
                restriction.baseTypeName,
                restriction.attributes
            )

            is XmlSchemaSimpleTypeRestriction -> Triple(
                restriction.facets,
                restriction.baseTypeName,
                listOf()
            )

            else -> throw Exception("Unknown restriction type: ${restriction.javaClass}")
        }

        // check for enumeration facets and exit without checking base type name
        // facet values must be strings and start with a letter
        if (facets.filterIsInstance<XmlSchemaEnumerationFacet>().isNotEmpty()) {
            val enumValues = facets.map {
                check(it.value.toString()[0].isLetter()) {
                    "Enumeration values must begin with a letter as we only support string enumerations. " +
                            "${it.value} does not"
                }
                it.value.toString()
            }.toList()

            val restrictionNode = createBaseNode(xmlNode, ENUM_TYPE_NAME)
            val nodeType = NodeType.StringEnumeration(restrictionNode, enumValues)
            restrictionNode.nodeType = nodeType
            return ReturnType.Message(restrictionNode)
        }

        // find the node for the base type and attach it to the node as a parameter
        val baseXmlNode = xmlNode.searchQName(baseTypeName)!!
        nodeMap[baseXmlNode]?.let { baseNode ->
            val parameterNode = createBaseNode(xmlNode, baseNode.nodeName, register = false)
            val parameter =
                NodeType.Parameter(parameterNode, baseNode.nodeType!!, restriction = extractRestrictions(facets))
            parameterNode.nodeType = parameter
            return ReturnType.Parameters(listOf(parameterNode))
        }

        // todo handle attributes here

        throw Exception("No base node found for $xmlNode")
    }

    /**
     * Generates [NodeType]s for all "root" types,  i.e. the one below the root node.
     *
     * Every other type is nested within those and will be covered by the generators while traversing.
     */
    private fun generateRootTypes(): List<BaseNode> {
        val nodes = mutableListOf<BaseNode>()

        tree.children.forEach { child ->
            when (BuiltinTypes.isBuiltinType(child.nodeName!!)) {
                true -> logger.debug { "Skip builtin type: ${child.nodeName}" }

                false -> when (child.element) {
                    is XmlSchemaSimpleType -> when (val result = generateSimpleType(child)) {
                        is ReturnType.Message -> {
                            logger.info("Generated simple type for ${result.node.nodeName}")
                            nodes.add(result.node)
                        }

                        is ReturnType.Parameters -> {
                            throw Exception("Cannot process anonymous complex type on root level")
                        }

                        else -> throw Exception("Unknown type $result")
                    }

                    is XmlSchemaComplexType -> when (val result = generateComplexType(child)) {
                        is ReturnType.Message -> {
                            logger.info("Generated complex type for ${result.node.nodeName}")
                            nodes.add(result.node)
                        }

                        is ReturnType.Parameters -> {
                            throw Exception("Cannot process anonymous complex type on root level")
                        }

                        else -> throw Exception("Unknown type $result")
                    }

                    is XmlSchemaElement -> {
                        val result = generateElement(child, child.element, isRootLevel = true)
                        logger.info("Generated element for ${result.nodeName}")
                        nodes.add(result)
                    }

                    is XmlSchemaAttributeGroup -> {
                        val result = generateAttributeGroup(child, child.element)
                        logger.info("Generated attribute group for ${result.nodeName}")
                        nodes.add(result)
                    }

                    is XmlSchemaAttribute -> when (val result = generateAttribute(child, child.element)) {
                        is ReturnType.Message -> {
                            logger.info("Generated attribute for ${result.node.nodeName}")
                            nodes.add(result.node)
                        }

                        is ReturnType.Parameters -> {
                            logger.info("Generated attribute for nodes ${result.nodes.joinToString { it.nodeName }}")
                            nodes.addAll(result.nodes)
                        }

                        else -> throw Exception("Unknown type $result")
                    }

                    else -> if (child.element != null) {
                        throw Exception("Handle unknown type ${child.element::class.simpleName}")
                    } else {
                        throw Exception("Cannot parse null element passed")
                    }
                }
            }
        }
        return nodes
    }

    /**
     * Determine for all used types whether they have extensions which can take their place as well.
     *
     * This is essentially building a forest of extensions, and determines which of these forests need to be used
     * in the place of the original type to model object oriented inheritance behavior.
     *
     * @param rootNode root of the tree in which to resolve inheritance
     * @return map in which the key is a node, and the values are all types which extend the base node
     */
    private fun buildInheritanceMap(rootNode: BaseNode): Map<BaseNode, List<BaseNode>> {
        // map in which the key is a base node, and the value is an extension
        val inheritanceMap = mutableMapOf<BaseNode, MutableSet<BaseNode>>()
        logger.info("Generating inheritance map")
        rootNode.traversePreOrder()
            // we only care about inheritance for nodes which are children of the root, or we would
            // generate OneOfs targeting nodes which are not in the scope of all types
            .filter { rootNode.children.contains(it) }
            .forEach { node ->
                when (val message = node.nodeType) {
                    is NodeType.Message -> {
                        message.extensionBaseNode?.let { baseNode ->
                            inheritanceMap.getOrPut(baseNode) { mutableSetOf() }.add(node)
                        }
                    }

                    else -> Unit
                }
            }

        logger.debug { "Inheritance map: $inheritanceMap" }

        val flattenInheritance = inheritanceMap.mapValues { (base, _) ->
            getExtensionNodes(inheritanceMap, base).toList().distinct()
        }

        logger.debug { "Flattened inheritance map: $flattenInheritance" }

        // determine which of the types is actually used in a parameter. (Hint: Probably all of them. Shit.)
        val usedInheritanceParameters = rootNode.traversePreOrder().mapNotNull { node ->
            when (val nodeType = node.nodeType) {
                is NodeType.Parameter -> {
                    if (flattenInheritance.containsKey(nodeType.parameterType.parent)) {
                        nodeType.parameterType.parent
                    } else {
                        null
                    }
                }

                else -> null
            }
        }.toList().distinct()
        logger.debug { "Actually used parameters: $usedInheritanceParameters" }

        return flattenInheritance
    }

    /**
     * Attaches newly generated OneOfs as nodes in the tree and replaces occurrences of the base type with OneOfs.
     *
     * @param rootNode root of the tree to which to attach OneOfs
     * @param inheritanceMap from which to derive new OneOf types
     */
    private fun attachInheritanceMap(rootNode: BaseNode, inheritanceMap: Map<BaseNode, List<BaseNode>>) {
        // generate nodes, attach them to the tree
        inheritanceMap.forEach { (baseNode, list) ->
            logger.info { "Creating inheritance OneOf for ${baseNode.nodeName}" }

            // create parameters for all types
            val parameters = list.map { node ->
                val parameterNode = createBaseNode(null, node.nodeName)
                val parameter = NodeType.Parameter(parameterNode, node.nodeType!!)
                parameterNode.nodeType = parameter
                parameterNode
            }.toList().sortedBy { it.nodeName }

            // base node is in parameters too
            val parameterBaseNode = createBaseNode(null, baseNode.nodeName)
            val baseNodeParameter = NodeType.Parameter(baseNode, baseNode.nodeType!!)
            parameterBaseNode.nodeType = baseNodeParameter

            // create message holding OneOfs
            val oneOfNode = createBaseNode(null, "${baseNode.nodeName}OneOf", null, register = false)
            val oneOf =
                NodeType.OneOf(oneOfNode, defaultTypeParameter = parameterBaseNode, baseNode.originalXmlNode?.qName)
            oneOfNode.nodeType = oneOf
            oneOfNode.children.addAll(listOf(parameterBaseNode) + parameters)

            val messageNode = createBaseNode(null, "${baseNode.nodeName}OneOf", register = false)
            val message = NodeType.Message(messageNode, qname = null)
            messageNode.nodeType = message
            messageNode.children.add(oneOfNode)

            // replace all occurrences of the base type as a parameter with this oneOf Node
            // except of course in the types which inherit from the base
            rootNode.children.forEach {
                replaceWithOneOfs(it, baseNode, message)
            }

            rootNode.children.add(messageNode)
        }
    }

    /**
     * Resolves cycles in the nodes by clustering nodes which have circular references.
     *
     * Languages need to handle these clusters when traversing the tree, as can be retrieved from [BaseNode.clusteredTypes]
     *
     * @param childNodes unresolved nodes containing cycles
     * @param resolved already resolved nodes
     * @return Pair of lists, in which the first list contains all still unresolved nodes, while the second list
     *  contains all previously resolved nodes as well as the removed clusters.
     *  TODO LDe: I don't think returning unresolved nodes makes sense, we are failing in this method on unresolved nodes
     */
    private fun resolveClusters(
        childNodes: List<BaseNode>,
        resolved: List<BaseNode>
    ): Pair<List<BaseNode>, List<BaseNode>> {
        // copy lists
        var rootChildNodes = childNodes.toMutableList()
        var resolvedNodes = resolved.toMutableList()

        // if there is something left, find the cycles causing it
        var clusterIterCountCount = 0
        while (rootChildNodes.isNotEmpty() && clusterIterCountCount <= RESOLUTION_ITERATIONS) {
            clusterIterCountCount++
            val deClusterBaseNode = rootChildNodes[0]
            val cycle = findCycle(deClusterBaseNode, mutableListOf())
                ?: throw Exception("Could not find cycle from node $deClusterBaseNode, you sure you didn't mess up before?")

            // the last element is the one that appeared twice, find the first occurrence
            val cycleEncounterNode = cycle[cycle.lastIndex]
            val firstIndex = cycle.indexOfFirst { it == cycleEncounterNode }

            val shortCycle: List<BaseNode> = cycle.subList(firstIndex, cycle.lastIndex)
            logger.warn {
                "Found cycle ${(shortCycle + cycleEncounterNode).joinToString(separator = " -> ") { it.nodeName }}," +
                        " clustering them"
            }

            logger.info("Setting types into a cluster")

            // add all nodes except the node itself to the cluster
            shortCycle.forEach { cycleNode ->
                cycleNode.clusteredTypes = (shortCycle - cycleNode).toList()
            }

            // now sort the nodes in the cycle alphabetically to have some sort of deterministic output
            val sortedShortCycle = shortCycle.sortedBy { it.nodeName }

            // retry resolution by removing one node from the cycle. try for each node until it works, or doesn't
            for (nodeToRemove in sortedShortCycle) {
                // move the base node into resolved types and check whether the resolving now finds a solution
                logger.debug { "Moving $nodeToRemove from unresolved to resolved types" }
                val resolvedCopy = resolvedNodes.toMutableList()
                val rootChildNodesCopy = rootChildNodes.toMutableList()

                resolvedCopy.add(nodeToRemove)
                rootChildNodesCopy.remove(nodeToRemove)

                // try solving again
                val (rootChildNodes2, resolvedNodes2) = resolveNodes(
                    iterations = RESOLUTION_ITERATIONS,
                    childNodes = rootChildNodesCopy,
                    resolved = resolvedCopy
                )

                if (resolvedCopy.size == resolvedNodes2.size) {
                    logger.info("Removing $nodeToRemove did not solve the clustering issue :(")
                    continue
                }

                // now check that the node we removed resolves as well
                val (_, resolvedNodes3) = resolveNodes(
                    iterations = 1,
                    childNodes = rootChildNodes2 + nodeToRemove,
                    resolved = resolvedNodes2 - nodeToRemove
                )

                require(resolvedNodes3.contains(nodeToRemove)) { "$nodeToRemove could still not be resolved, I'm out" }

                // cluster resolved, update nodes
                rootChildNodes = rootChildNodes2
                resolvedNodes = resolvedNodes2
                break
            }
        }

        return Pair(rootChildNodes, resolvedNodes)
    }

    /**
     * Replaces all occurrences of the base type as a parameter with a oneOf Node
     *
     * Except of course in the types which inherit from the base.
     *
     * @param node in which to insert OneOfs
     * @param baseNode the node which is to be replaced with a OneOf type
     * @param message the OneOf to use for occurrences of [baseNode]
     */
    private fun replaceWithOneOfs(node: BaseNode, baseNode: BaseNode, message: NodeType.Message) {
        node.children.forEach { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Message -> {
                    replaceWithOneOfs(childNode, baseNode, message)
                }

                is NodeType.Parameter -> {
                    var mustReplace = nodeType.parameterType.parent == baseNode
                    val parentMessage = node.nodeType as NodeType.Message // has to be!
                    // skip nodes which are actually extensions of our type, we don't want to replace there
                    mustReplace = mustReplace && parentMessage.extensionBaseNode != baseNode

                    if (mustReplace) {
                        logger.info { "Replacing parameter type ${baseNode.nodeName} in ${node.nodeName} with ${baseNode.nodeName}OneOf" }
                        nodeType.parameterType = message
                    }
                }

                else -> Unit
            }
        }
    }

    /**
     * Resolves all nodes and puts them in an order matching their resolution, i.e. sorted so dependencies come before their use.
     *
     * _Caveat: Does not handle cycles! See [resolveClusters] and [findCycle]._
     *
     * @param childNodes nodes which are not yet resolved
     * @param resolved nodes which are already resolved (i.e. builtins or previous runs before removing a cluster)
     * @return Pair of lists, in which the first list contains all still unresolved nodes, while the second list
     *  contains all previously and now resolved nodes in the correct order.
     */
    private fun resolveNodes(
        childNodes: List<BaseNode>,
        resolved: List<BaseNode>,
        iterations: Int
    ): Pair<MutableList<BaseNode>, MutableList<BaseNode>> {
        var iterationCount = 0
        var rootChildNodesCopy = childNodes.toMutableList()
        val resolvedNodesCopy = resolved.toMutableList()
        while (rootChildNodesCopy.isNotEmpty() && iterationCount < iterations) {
            rootChildNodesCopy = rootChildNodesCopy.mapNotNull { node ->
                if (isFullyResolvedNode(node, resolvedNodesCopy)) {
                    resolvedNodesCopy.add(node)
                    null
                } else {
                    node
                }
            }.toMutableList()
            iterationCount++
            logger.info("Reordering tree loop round $iterationCount done, resolved ${resolvedNodesCopy.size}, remaining ${rootChildNodesCopy.size}")
        }

        return Pair(rootChildNodesCopy, resolvedNodesCopy)
    }

    /**
     * Generates a sequence of all extensions for a given node.
     *
     * @param map of all known extension
     * @param node to find all extensions for
     * @return sequence of all extensions applicable to the node
     */
    private fun getExtensionNodes(map: Map<BaseNode, Set<BaseNode>>, node: BaseNode): Sequence<BaseNode> {
        return sequence {
            map[node]?.let { extensionNodes ->
                extensionNodes.forEach { extensionNode ->
                    yield(extensionNode)
                    yieldAll(getExtensionNodes(map, extensionNode))
                }
            }
        }
    }

    /**
     * Determines whether a parameter has already been resolved.
     *
     * @param node parent node of the parameter, used to check whether siblings are the parameter type
     * @param nodeType the parameter to check for
     * @param resolvedNodes list of already resolved nodes
     * @return true if parameter is resolved, false otherwise
     */
    private fun resolveParameter(node: BaseNode, nodeType: NodeType.Parameter, resolvedNodes: List<BaseNode>): Boolean {
        // resolved externally
        var resolved = resolvedNodes.contains(nodeType.parameterType.parent)
        logger.debug { "${node.nodeName} has resolved type: $resolved" }
        // builtin type
        resolved = resolved || nodeType.parameterType is NodeType.BuiltinType
        logger.debug { "${node.nodeName} has builtin (or resolved) type: $resolved" }
        // resolved within the same scope of the message
        resolved = resolved || node.children.contains(nodeType.parameterType.parent)
        logger.debug { "${node.nodeName} has nested (or builtin or resolved) type: $resolved" }
        if (!resolved) {
            logger.info {
                "Could not resolve parameter ${nodeType.parent?.nodeName} in ${node.nodeName}," +
                        " type ${nodeType.parameterType.parent} was unresolved"
            }
        }
        return resolved
    }

    /**
     * Determines whether a node has been fully resolved.
     *
     * _This can only be used for message nodes!_
     *
     * @param node to resolve message for
     * @param resolvedNodes list of already resolved nodes
     * @return true if message is fully resolved, false otherwise
     */
    private fun isFullyResolvedNode(node: BaseNode, resolvedNodes: List<BaseNode>): Boolean {
        val filtered = node.children.filter { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Parameter -> {
                    !resolveParameter(node, nodeType, resolvedNodes)
                }

                is NodeType.Message -> {
                    !isFullyResolvedNode(childNode, resolvedNodes)
                }

                is NodeType.OneOf -> {
                    !isFullyResolvedNode(childNode, resolvedNodes)
                }

                is NodeType.BuiltinType, is NodeType.StringEnumeration -> false // remove nodes which have no deps
                else -> true // unknown types should not be resolved blindly
            }
        }.toList()
        return filtered.isEmpty()
    }

    /**
     * Recursively finds the *longest* cycle starting from the given base node.
     *
     * @param node to start cycle search from
     * @param visited list containing the current search path of nodes
     * @return list of nodes mapping the path which was used do find the cycle, null if no cycle was found
     */
    private fun findCycle(node: BaseNode, visited: MutableList<BaseNode>): List<BaseNode>? {
        // we don't care about OneOfs, they're nested inside messages, we count those
        if (node.nodeType !is NodeType.OneOf) {
            if (visited.contains(node)) {
                // cycle found, add this node so we can determine the cycle in the return value
                visited.add(node)
                return visited
            }
            visited.add(node)
        }

        // collect all cycles, find the longest  one
        return node.children.mapNotNull { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Parameter -> {
                    findCycle(nodeType.parameterType.parent!!, visited.toMutableList())
                }

                is NodeType.Message -> {
                    findCycle(childNode, visited.toMutableList())
                }

                is NodeType.OneOf -> {
                    findCycle(childNode, visited.toMutableList())
                }

                else -> null
            }
        }.takeIf { it.isNotEmpty() }
            // use longest cycle
            ?.reduce { acc, list -> if (acc.size > list.size) acc else list }
    }

    companion object : Logging {
        const val RESOLUTION_ITERATIONS: Int = 10
        const val ENUM_TYPE_NAME: String = "EnumType"

        fun isList(element: XmlSchemaElement) = element.maxOccurs > 1L

        fun isOptional(element: XmlSchemaElement) = element.minOccurs == 0L
    }
}