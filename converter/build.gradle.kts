plugins {
    id("org.somda.sdc.protosdc_converter.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
}

dependencies {
    implementation(projects.kotlinProtoBaseMappers)
    implementation(projects.kotlinXmlBaseMappers)
    implementation(projects.xmlprocessor)

    implementation(libs.clikt)
    implementation(libs.ktoml.core)
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    // To add all of the dependencies otherwise a "NoClassDefFoundError" error
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)

    manifest {
        attributes["Main-Class"] = "org.somda.protosdc_converter.converter.MainKt"
    }

    // This line of code recursively collects and copies all of a project's files
    // and adds them to the JAR itself.
    // This resolves the following issue when launching the jar:
    // Exception in thread "main" java.lang.NoClassDefFoundError: kotlin/jvm/internal/Intrinsics
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
}
