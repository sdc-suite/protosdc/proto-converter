package org.somda.protosdc_converter.converter.kotlinmapper.xml

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.kotlin.*
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.converter.kotlinmapper.findBicepsExtensions
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.EpisodeData
import java.io.ByteArrayOutputStream
import java.io.PrintWriter
import javax.xml.namespace.QName

const val KOTLIN_XML_BICEPS_EXTENSION_TOML_TABLE_NAME = "KotlinXmlExtensions"

/**
 * Generates object class that maps known extensions from XML to Kotlin and vice versa.
 */
class KotlinXmlExtensionsPlugin(
    tree: BaseNode,
    converterConfig: ConverterConfig,
    kotlinConfig: KotlinConfig,
    private val kotlinXmlMappingConfig: KotlinXmlMappingConfig,
    private val kotlinXmlExtensionsConfig: KotlinXmlExtensionsConfig,
    customExtensionNodes: List<BaseNode>,
    private val mapperUtil: KotlinXmlMapperUtil
) : BaseNodePlugin {
    private val kotlinSource = KotlinSource(
        kotlinConfig.indentationSize.toInt(),
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val allExtensionNodes = tree.findBicepsExtensions() + customExtensionNodes

    private val outputFolders = setupOutputFolders(
        converterConfig.outputFolder,
        kotlinXmlExtensionsConfig.outputFolders,
        kotlinXmlExtensionsConfig.outputFoldersNoHierarchy
    ) { "Output folders are missing for Kotlin BICEPS Extensions Handler generator" }

    override fun run() {
        // no namespace, no mapper to write
        val namespace = resolveNamespace()?.namespaceURI ?: return

        // determine directory structure required based on package
        val filesToWrite = determineFilesToWrite(
            outputFolders,
            kotlinXmlExtensionsConfig.mappingPackage,
            appendKotlinFileExtension(kotlinXmlExtensionsConfig.outputClassName)
        )

        val byteStream = ByteArrayOutputStream()
        PrintWriter(byteStream).use { printWriter ->
            // print package declaration
            printWriter.println(kotlinSource.packageDeclaration(kotlinXmlExtensionsConfig.mappingPackage))
            printWriter.println()

            // no imports needed, every type is rendered fully qualified

            // begin object class, both mapping directions are included
            printWriter.println(
                when (val ifName = kotlinXmlExtensionsConfig.fullyQualifiedInterfaceName) {
                    null -> kotlinSource.objectStart(kotlinXmlExtensionsConfig.outputClassName)
                    else -> kotlinSource.objectStart("${kotlinXmlExtensionsConfig.outputClassName} : $ifName")
                }
            )

            printWriter.println(kotlinSource.indent(1) {
                prependOverride("""fun namespace() = "$namespace"""")
            })
            printWriter.println()

            // generate mapping direction Proto -> Kotlin
            writeXmlToKotlinFunctions(printWriter)

            // generate mapping direction Kotlin -> Proto
            writeKotlinToXmlFunctions(printWriter)

            printWriter.println(kotlinSource.blockEnd())
        }

        writeByteStreamToFile(byteStream, filesToWrite)
    }

    private fun writeXmlToKotlinFunctions(target: PrintWriter) {
        target.println(kotlinSource.indent(1) {
            prependOverride(xmlToKotlinFunctionStart())
        })

        // loop all extensions
        for (node in allExtensionNodes) {
            val xmlType = mapperUtil.fullyQualifiedXmlNameFor(node)
            val kotlinType = mapperUtil.fullyQualifiedKotlinNameFor(node)
            val extensionMapperFunction = KotlinSource.qualifiedPathFrom(
                kotlinXmlMappingConfig.mappingPackage,
                kotlinXmlMappingConfig.xmlToKotlinClassName,
                mapperUtil.xmlToKotlinMapperMap[xmlType, kotlinType]!!
            )

            val qName = QName.valueOf(xmlType)
            target.println(kotlinSource.indentLines(2) {
                listOf(
                    CodeLine(
                        indentOffset = 0,
                        line = kotlinSource.ifStart(
                            """$SOURCE.namespaceURI == "${qName.namespaceURI}" && """ +
                                    """$SOURCE.localName == "${qName.localPart}""""
                        )
                    ),
                    CodeLine(
                        indentOffset = 1,
                        line = kotlinSource.returnAny(
                            "$extensionMapperFunction($SOURCE)"
                        )
                    ),
                    CodeLine(
                        indentOffset = 0,
                        line = kotlinSource.blockEnd()
                    )
                )
            })
        }

        // callback for else branch
        target.println(kotlinSource.indent(2) {
            kotlinSource.returnAny("null")
        })

        // close blocks
        target.println(kotlinSource.indent(1) {
            kotlinSource.blockEnd()
        })

        target.println()
    }

    private fun writeKotlinToXmlFunctions(target: PrintWriter) {
        target.println(kotlinSource.indent(1) {
            prependOverride(kotlinToXmlFunctionStart())
        })
        target.println(kotlinSource.indent(2) {
            kotlinSource.returnAny(kotlinSource.whenStart(SOURCE))
        })

        // loop all extensions
        for (node in allExtensionNodes) {
            val xmlType = mapperUtil.fullyQualifiedXmlNameFor(node)
            val kotlinType = mapperUtil.fullyQualifiedKotlinNameFor(node)
            target.println(kotlinSource.indentLines(3) {
                // map two known types
                val extensionMapperFunction = KotlinSource.qualifiedPathFrom(
                    kotlinXmlMappingConfig.mappingPackage,
                    kotlinXmlMappingConfig.kotlinToXmlClassName,
                    mapperUtil.kotlinToXmlMapperMap[kotlinType, xmlType]!!
                )

                // this is an official extension since a mustUnderstand attribute is available
                // create straightforward mapping
                listOf(
                    CodeLine(
                        kotlinSource.whenEntry(
                            condition = "is $kotlinType",
                            statement = kotlinSource.blockStart().trim()
                        )
                    ),
                    CodeLine(1, QName.valueOf(xmlType).let { qName ->
                        kotlinSource.assignment(
                            "val target",
                            """$TARGET_OWNER.createElementNS("${qName.namespaceURI}", "${qName.localPart}")"""
                        )
                    }),
                    CodeLine(1, "$extensionMapperFunction($SOURCE, target)"),
                    CodeLine(kotlinSource.blockEnd())
                )
            })
        }

        // callback for else branch
        target.println(kotlinSource.indent(3) {
            kotlinSource.whenEntry(
                condition = "else",
                statement = "null"
            )
        })

        // close blocks
        repeat(2) {
            target.println(kotlinSource.indent(2 - it) {
                kotlinSource.blockEnd()
            })
        }
        target.println()
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        throw Exception("${this.javaClass} does not provide ${EpisodeData::class.java}")
    }

    override fun pluginIdentifier() = "kotlinxmlbicepsextensionsv1"

    private fun xmlToKotlinFunctionStart() =
        "fun $MAP_TO_KOTLIN($SOURCE: ${KotlinType.DOM_NODE}): ${KotlinType.nullable(KotlinType.KOTLIN_ANY)} {"

    private fun kotlinToXmlFunctionStart() =
        "fun $MAP_TO_XML($SOURCE: ${KotlinType.KOTLIN_ANY}, $TARGET_OWNER: ${KotlinType.DOM_DOCUMENT}): ${
            KotlinType.nullable(
                KotlinType.DOM_NODE
            )
        } {"

    private fun prependOverride(line: String) = when (kotlinXmlExtensionsConfig.fullyQualifiedInterfaceName) {
        null -> line
        else -> "override $line"
    }

    private fun resolveNamespace(): QName? {
        val qName = allExtensionNodes.firstOrNull()?.let { firstNode ->
            firstNode.originalXmlNode?.qName!!
        } ?: return null

        require(allExtensionNodes.filterNot { it.originalXmlNode?.qName?.namespaceURI == qName.namespaceURI }
            .isEmpty()) {
            "All extensions need to share the same namespace."
        }

        return qName
    }

    companion object {
        private const val SOURCE = "source"
        private const val TARGET_OWNER = "targetOwner"
        private const val MAP_TO_KOTLIN = "mapKnownToKotlin"
        private const val MAP_TO_XML = "mapKnownToXml"

        fun parseConfig(config: String): KotlinXmlExtensionsConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, KOTLIN_XML_BICEPS_EXTENSION_TOML_TABLE_NAME)
        }
    }
}

