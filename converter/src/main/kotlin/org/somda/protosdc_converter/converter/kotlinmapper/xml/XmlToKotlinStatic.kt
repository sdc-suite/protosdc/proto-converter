package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.w3c.dom.Node

/**
 * Utility function to simulate a text node.
 */
object TextNodeCreator {
    operator fun invoke() = listOf(
        CodeLine("fun $FUNCTION_NAME(node: ${KotlinType.DOM_NODE}, text: String): ${KotlinType.DOM_NODE} {"),
        CodeLine(1, "return object: ${KotlinType.DOM_NODE} by node {"),
        CodeLine(2, "override fun getTextContent(): String = text"),
        CodeLine(1, "}"),
        CodeLine("}")
    )

    const val FUNCTION_NAME = "textNodeFrom"
}

/**
 * Utility function to create a [List] facade from a [org.w3c.dom.NodeList].
 *
 * Caution: the facade does not implement all members, only those that are necessary to transform to sequences.
 */
object NodeListToList {
    operator fun invoke() = listOf(
        CodeLine("class NodeListListIterator(private val nodeList: org.w3c.dom.NodeList, init: Int = 0) : ListIterator<org.w3c.dom.Node> {"),
        CodeLine(1, "var index = init"),
        CodeLine(""),
        CodeLine(1, "override fun hasNext(): Boolean { return index < nodeList.length }"),
        CodeLine(""),
        CodeLine(1, "override fun hasPrevious(): Boolean { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(1, "override fun next(): org.w3c.dom.Node { return nodeList.item(index++) }"),
        CodeLine(""),
        CodeLine(1, "override fun nextIndex(): Int { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(1, "override fun previous(): org.w3c.dom.Node { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(1, "override fun previousIndex(): Int { TODO(\"Not yet implemented\") }"),
        CodeLine("}"),
        CodeLine(""),
        CodeLine("fun listFacade(nodeList: org.w3c.dom.NodeList): List<org.w3c.dom.Node> {"),
        CodeLine(1, "return object: List<org.w3c.dom.Node> {"),
        CodeLine(2, "override val size: Int"),
        CodeLine(3, "get() = nodeList.length"),
        CodeLine(""),
        CodeLine(2, "override fun get(index: Int): org.w3c.dom.Node { return nodeList.item(index) }"),
        CodeLine(""),
        CodeLine(2, "override fun isEmpty(): Boolean { return nodeList.length == 0 }"),
        CodeLine(""),
        CodeLine(2, "override fun iterator(): Iterator<org.w3c.dom.Node> { return NodeListListIterator(nodeList) }"),
        CodeLine(""),
        CodeLine(2, "override fun listIterator(): ListIterator<org.w3c.dom.Node> { return NodeListListIterator(nodeList) }"),
        CodeLine(""),
        CodeLine(
            2,
            "override fun listIterator(index: Int): ListIterator<org.w3c.dom.Node> { return NodeListListIterator(nodeList, index) }"
        ),
        CodeLine(""),
        CodeLine(2, "override fun subList(fromIndex: Int, toIndex: Int): List<org.w3c.dom.Node> { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(2, "override fun lastIndexOf(element: org.w3c.dom.Node): Int { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(2, "override fun indexOf(element: org.w3c.dom.Node): Int { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(2, "override fun containsAll(elements: Collection<org.w3c.dom.Node>): Boolean { TODO(\"Not yet implemented\") }"),
        CodeLine(""),
        CodeLine(2, "override fun contains(element: org.w3c.dom.Node): Boolean { TODO(\"Not yet implemented\") }"),
        CodeLine(1, "}"),
        CodeLine("}")
    )

    const val FUNCTION_NAME = "listFacade"
}


/**
 * Utility function to split strings at whitespaces.
 */
object ListSplit {
    operator fun invoke() = listOf(
        CodeLine("private val whitespaceRegex = \"\\\\s+\".toRegex()"),
        CodeLine(""),
        CodeLine("fun $FUNCTION_NAME(text: String): List<String> {"),
        CodeLine(1, "return text.split(whitespaceRegex)"),
        CodeLine("}")
    )

    const val FUNCTION_NAME = "splitAtWhitespace"
}