package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlWriterConfig
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.SELF
import org.somda.protosdc_converter.converter.rust.WRITER_ERROR
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.callPrimitiveWriter
import org.somda.protosdc_converter.converter.rust.callSimpleWriter
import org.somda.protosdc_converter.converter.rust.simpleWriterSignature
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class SimpleTypeWriter(
    val writerName: String,
    val dataType: BaseNode,
    val primitiveWriter: RustWriter?,
    val simpleWriter: RustWriter?,
) : WriterTracker {

    init {
        check((primitiveWriter != null).xor(simpleWriter != null))
    }

    override fun generateWriter(
        config: QuickXmlWriterConfig,
        primitiveWriters: MutableMap<BaseNode, RustWriter>,
        simpleWriters: MutableMap<BaseNode, RustWriter>,
        complexWriters: MutableMap<BaseNode, RustWriter>
    ): String {
        check(dataType.children.size == 1)

        val dataTypeRustStruct = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

        val parameter = dataType.children[0]
        val rustParameter = parameter.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter

        val fullyQualifiedRustTypeName = dataTypeRustStruct.fullyQualifiedName(config.modelRustModuleSubstitute)

        val valueVariable = "written"

        val fieldName = "$SELF.${rustParameter.parameterName}"

        val writerCall: (String) -> String = when {
            primitiveWriter != null -> { variable -> callPrimitiveWriter(config, primitiveWriter, "&$variable") }
            else -> { variable -> callSimpleWriter(variable) }
        }

        val writtenCall: String = when {
            !rustParameter.list && !rustParameter.optional -> "${INDENT}let $valueVariable = ${writerCall(fieldName)}?;"
            rustParameter.list && !rustParameter.optional -> {
                """${INDENT}let $valueVariable = $fieldName.iter()
                        |${INDENT.repeat(3)}.map(|it| ${writerCall("it")})
                        |${INDENT.repeat(3)}.collect::<Result<Vec<_>, $WRITER_ERROR>>()?
                        |${INDENT.repeat(3)}.join(b" ".as_slice());""".trimMargin()
            }

            else -> run {
                TODO("$rustParameter list ${rustParameter.list} optional ${rustParameter.optional}")
            }
        }

        return """${simpleWriterSignature(fullyQualifiedRustTypeName)}
${INDENT.repeat(2)}$writtenCall
${INDENT.repeat(3)}Ok($valueVariable)
${INDENT.repeat(2)}}
}"""
    }
}

