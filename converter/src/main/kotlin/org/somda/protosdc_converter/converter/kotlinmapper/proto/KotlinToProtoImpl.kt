package org.somda.protosdc_converter.converter.kotlinmapper.proto

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.Proto3Generator
import org.somda.protosdc_converter.converter.kotlin.KotlinEntry
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlin.kotlinLanguageType
import org.somda.protosdc_converter.converter.kotlin.typeName
import org.somda.protosdc_converter.converter.protoLanguageType
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import java.io.PrintWriter

/**
 * Generates mapping functions for the mapping direction Kotlin -> Proto.
 */
internal class KotlinToProtoImpl(
    private val kotlinSource: KotlinSource,
    private val kotlinMappingSource: KotlinProtoMappingSource,
    private val util: KotlinProtoMapperUtil,
    private val oneOfClassPrefix: String
) {
    private val kotlinToProtoMapperMap = util.kotlinToProtoMapperMap

    /**
     * Prints the mapping functions related to the given node to the given target print writer.
     *
     * @param node the root base node or currently processed node.
     * @param target the writer to which code will be generated.
     * @param level indentation offset for output.
     * @param parentNode parent node if present.
     */
    fun processNode(node: BaseNode, target: PrintWriter, level: Int = 0, parentNode: BaseNode? = null) {
        if (node.ignore) {
            return
        }

        val protoLanguageType = node.protoLanguageType()
        val kotlinLanguageType = node.kotlinLanguageType()

        // so we need to consider a couple of cases here
        // 1. This is a top level element which has the full name as its type name
        // 2. This is a nested element which is only defined within the context of the parent message
        // 3. This is a parameter which points to
        //    1. A top level element
        //    2. A nested message which is defined within the same context

        val qualifiedProtoTypeName = util.protoToKotlinType(util.fullyQualifiedProtoNameFor(node))
        val qualifiedKotlinTypeName = util.fullyQualifiedKotlinNameFor(node)

        node.clusteredTypes?.let { cluster ->
            if (cluster.isEmpty()) {
                return@let
            }
            logger.debug { "Encountered cluster ${node.clusteredTypes}" }

            if (node.kotlinToProtoClusterHandled()) {
                logger.debug { "Encountered cluster ${node.clusteredTypes} is already handled" }
                return@let
            }

            logger.debug { "Encountered cluster ${node.clusteredTypes} is new" }
            logger.debug { "Add cluster mappers all at once to map" }

            (listOf(node) + cluster).forEach { clusterType ->
                clusterType.kotlinToProtoSetClusterHandled()

                // create mappers
                val protoName = util.fullyQualifiedProtoNameFor(clusterType)
                val kotlinName = util.fullyQualifiedKotlinNameFor(clusterType)
                val functionName = kotlinMappingSource.mapFunctionName(
                    KotlinProtoMappingSource.functionNameOfPackage(
                        kotlinName
                    )
                )
                kotlinToProtoMapperMap[kotlinName, protoName] = functionName
            }
        }

        // lookup if mapper is already present
        val mapper = kotlinToProtoMapperMap[qualifiedKotlinTypeName, qualifiedProtoTypeName]

        when (
            val nodeType = node.nodeType) {
            is NodeType.Message -> {

                if (mapper != null && node.clusteredTypes == null) {
                    logger.debug {
                        "Skip generating mapper from non-clustered message $qualifiedProtoTypeName to " +
                                "$qualifiedKotlinTypeName, already present"
                    }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toSet()
                val nestedTypes = node.children - parameters

                val kotlinLanguageTypeSkip = when (kotlinLanguageType) {
                    is KotlinEntry.DataClass -> kotlinLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in kotlin
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && kotlinLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNode(child, target, level, node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName =
                    kotlinMappingSource.mapFunctionName(
                        KotlinProtoMappingSource.functionNameOfPackage(
                            qualifiedKotlinTypeName
                        )
                    )

                kotlinToProtoMapperMap[qualifiedKotlinTypeName, qualifiedProtoTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.mapFunctionStart(
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.protoBuilderStart(qualifiedProtoTypeName)
                })

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNode(child, target, level + 1, node)
                    }
                }

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.protoBuilderEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })

            }

            is NodeType.Parameter -> {
                check(qualifiedProtoTypeName == qualifiedKotlinTypeName || mapper != null) {
                    "Cannot map from parameter $qualifiedKotlinTypeName to $qualifiedProtoTypeName, no mapper!"
                }

                when {
                    nodeType.list -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged

                        target.println(kotlinSource.indent(level) {
                            kotlinMappingSource.protoFromKotlinList(
                                protoField = util.getProtoFieldName(protoLanguageType, pascalCase = true),
                                kotlinField = util.getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        })
                    }

                    nodeType.optional -> {
                        target.println(kotlinSource.indent(level) {
                            kotlinMappingSource.protoFieldFromOptionalKotlin(
                                protoField = util.getProtoFieldName(protoLanguageType),
                                kotlinField = util.getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        })
                    }

                    else -> {
                        target.println(kotlinSource.indent(level) {
                            kotlinMappingSource.protoFieldFromKotlin(
                                protoField = util.getProtoFieldName(protoLanguageType),
                                kotlinField = util.getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        })
                    }
                }
            }

            is NodeType.StringEnumeration -> {

                // create mapper
                val functionName =
                    kotlinMappingSource.mapFunctionName(
                        KotlinProtoMappingSource.functionNameOfPackage(
                            qualifiedKotlinTypeName
                        )
                    )

                kotlinToProtoMapperMap[qualifiedKotlinTypeName, qualifiedProtoTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.mapFunctionStart(
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.kotlinFromProtoEnumStart()
                })

                nodeType.values.forEach { value ->
                    target.println(kotlinSource.indent(level + 2) {
                        kotlinMappingSource.kotlinFromProtoEnumCase(
                            sourceType = qualifiedKotlinTypeName,
                            sourceValue = value,
                            targetType = qualifiedProtoTypeName,
                            targetValue = util.toProtoEnumName(value)
                        )
                    })
                }

                target.println(kotlinSource.indent(level + 2) {
                    kotlinMappingSource.kotlinFromProtoEnumElse()
                })
                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.kotlinFromProtoEnumEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            is NodeType.BuiltinType -> {
                logger.debug { "Skipping builtin type ${node.nodeName}" }
            }

            is NodeType.OneOf -> {
                check(node.kotlinLanguageType() is KotlinEntry.KotlinOneOf)
                check(node.protoLanguageType() is Proto3Generator.ProtoEntry.ProtoOneOf)

                // retrieve the fully qualified parent node name here for proto
                val oneOfQualifiedProtoTypeName = util.fullyQualifiedProtoNameFor(parentNode!!)

                // create mapper
                val functionName =
                    kotlinMappingSource.mapFunctionName(
                        KotlinProtoMappingSource.functionNameOfPackage(
                            qualifiedKotlinTypeName
                        )
                    )

                kotlinToProtoMapperMap[qualifiedKotlinTypeName, oneOfQualifiedProtoTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.mapFunctionStart(
                        functionName = functionName,
                        targetType = oneOfQualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.protoFromKotlinOneOfStart()
                })

                node.children.forEach { child ->
                    check(child.nodeType is NodeType.Parameter)

                    val kotlinChild = child.languageType[OutputLanguage.Kotlin]
                    val protoChild = child.languageType[OutputLanguage.Proto]

                    check(kotlinChild is KotlinEntry.KotlinOneOfParameter)
                    check(protoChild is Proto3Generator.ProtoEntry.ProtoParameter)

                    val childProtoTypeName = util.protoToKotlinType(util.fullyQualifiedProtoNameFor(child))
                    val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                    // generate oneof parameters
                    val childMapper = kotlinToProtoMapperMap[childKotlinTypeName, childProtoTypeName]

                    check(childProtoTypeName == childKotlinTypeName || childMapper != null) {
                        "Cannot map from oneof parameter $childKotlinTypeName to $childProtoTypeName, no mapper!"
                    }

                    val protoFieldName = util.getProtoFieldName(protoChild, pascalCase = true)

                    val kotlinChoiceName = kotlinChild.kotlinType.typeName()
                    val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                        qualifiedKotlinTypeName,
                        "$oneOfClassPrefix$kotlinChoiceName"
                    )

                    target.println(kotlinSource.indent(level + 2) {
                        kotlinMappingSource.protoFromKotlinOneOfCase(
                            protoCase = oneOfQualifiedProtoTypeName,
                            protoField = protoFieldName,
                            kotlinChoice = "is $kotlinCaseName",
                            mappingFunction = childMapper
                        )
                    })
                }

                target.println(kotlinSource.indent(level + 2) {
                    kotlinMappingSource.kotlinFromProtoEnumElse()
                })
                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.kotlinFromProtoEnumEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            else -> throw Exception("Cannot handle nodeType ${node.nodeType}")
        }
    }

    private companion object : Logging
}