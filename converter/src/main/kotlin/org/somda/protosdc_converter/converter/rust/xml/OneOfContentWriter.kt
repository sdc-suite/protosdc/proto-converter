package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlWriterConfig
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.SELF
import org.somda.protosdc_converter.converter.rust.TAG_NAME
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.callElementWriter
import org.somda.protosdc_converter.converter.rust.elementWriterSignature
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class OneOfContentWriter(val writerName: String, val dataType: BaseNode) : WriterTracker {
    override fun generateWriter(
        config: QuickXmlWriterConfig,
        primitiveWriters: MutableMap<BaseNode, RustWriter>,
        simpleWriters: MutableMap<BaseNode, RustWriter>,
        complexWriters: MutableMap<BaseNode, RustWriter>
    ): String {
        val targetType = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct
        val targetTypeName = targetType.fullyQualifiedName(config.modelRustModuleSubstitute)

        val oneOfNode = dataType.children[0]
        val oneOfElements = oneOfNode.children

        val middle = generateOneOfMiddle(oneOfElements, targetTypeName, primitiveWriters, simpleWriters, complexWriters)

        val start = """
${elementWriterSignature(targetTypeName)}
        match $SELF {
"""

        val end = """
        }
    }
}"""

        return start + middle + end
    }


    companion object {
        fun generateOneOfMiddle(
            oneOfElements: List<BaseNode>,
            targetTypeName: String,
            primitiveWriters: MutableMap<BaseNode, RustWriter>,
            simpleWriters: MutableMap<BaseNode, RustWriter>,
            complexWriters: MutableMap<BaseNode, RustWriter>
        ): String = oneOfElements.joinToString(separator = "\n") { element ->

            val variantType = (element.nodeType as NodeType.Parameter).parameterType.parent!!
            val variantRustType =
                variantType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

            val variantIsSimpleType = simpleWriters[variantType] != null
            val variantWriter = complexWriters[variantType]

            val param = element.languageType[OutputLanguage.Rust] as RustEntry.RustOneOfParameter
            when (variantIsSimpleType) {
                false -> {
                    checkNotNull(variantWriter) { "$variantType writer missing" }
                    val variable = when (param.box) {
                        true -> "(*it)"
                        false -> "it"
                    }
                    """${INDENT.repeat(3)}$targetTypeName::${element.nodeName}(it) => ${
                        callElementWriter(
                            variable,
                            TAG_NAME,
                            xsiType = true
                        )
                    },"""
                }

                true -> "${INDENT.repeat(3)}$targetTypeName::${element.nodeName}(it) => panic!(\"simple type variant not implemented\"),"
            }
        }
    }
}
