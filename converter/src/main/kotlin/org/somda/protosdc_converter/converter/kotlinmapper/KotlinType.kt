package org.somda.protosdc_converter.converter.kotlinmapper

object KotlinType {
    const val QNAME = "javax.xml.namespace.QName"
    const val DOM_NODE = "org.w3c.dom.Node"
    const val DOM_NODE_LIST = "org.w3c.dom.NodeList"
    const val DOM_ELEMENT = "org.w3c.dom.Element"
    const val DOM_DOCUMENT = "org.w3c.dom.Document"
    const val KOTLIN_ANY = "kotlin.Any"
    const val EXCEPTION = "kotlin.Exception"
    const val PROTOBUF_ANY = "com.google.protobuf.Any"

    fun nullable(type: String) = "$type?"
}