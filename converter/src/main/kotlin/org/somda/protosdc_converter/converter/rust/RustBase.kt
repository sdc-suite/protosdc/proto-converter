package org.somda.protosdc_converter.converter.rust

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.Proto3Generator
import org.somda.protosdc_converter.converter.getNodeForProtoEntry
import org.somda.protosdc_converter.converter.protoEntryOf
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.BuiltinTypes
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import org.somda.protosdc_converter.xmlprocessor.XmlType

internal fun String.camelToSnakeCase() = fold(StringBuilder(length)) { acc, c ->
    if (c in 'A'..'Z') (if (acc.isNotEmpty()) acc.append('_') else acc).append(c + ('a' - 'A'))
    else acc.append(c)
}.toString()


internal fun String.replacePrefixWithCrate(segment: String?): String {
    return when (segment != null && this.startsWith(segment)) {
        true -> this.replace(segment, "crate")
        false -> this
    }
}

@kotlinx.serialization.Serializable
data class CallOverride(val module: String, val name: String) {
    fun fullyQualified(replacementPrefix: String?) = "${module.replacePrefixWithCrate(replacementPrefix)}$MOD_SEP$name"
}


class RustBase(
    val protoModelModule: String,
    val tree: BaseNode,
    val protoCustomTypesTree: BaseNode
) {

    companion object: Logging {
        fun protoRustModuleName(data: String): String {
            return data.camelToSnakeCase().replace("::_", "::")
        }



        fun getRustEntry(node: BaseNode): RustEntry {
            return node.languageType[OutputLanguage.Rust] as RustEntry
        }
    }


    private fun getParentForNode(node: BaseNode): BaseNode? {
        if (tree.children.contains(node)) {
            return tree
        }
        tree.traversePreOrder().forEach { child ->
            // referential equality, yay
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        // TODO LDe: This shouldn't be, it's hacky
        protoCustomTypesTree.traversePreOrder().forEach { child ->
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        return null
    }

    fun getProtoParameterName(baseNode: BaseNode): String {
        return when (val languageType = baseNode.languageType[OutputLanguage.Proto]) {
            is Proto3Generator.ProtoEntry.ProtoParameter -> languageType.parameterName
            else -> TODO("proto languageType $baseNode.languageType[OutputLanguage.Proto]")
        }
    }

    fun isPrimitiveProtoType(nodeType: NodeType.Parameter): Boolean = nodeType.parameterType.parent!!.languageType[OutputLanguage.Proto]?.let {
        Proto3Generator.BUILT_IN_PRIMITIVE_TYPES.containsValue(it) || Proto3Generator.OPTIONAL_PRIMITIVE_COMPANIONS.containsValue(it)
    } ?: false

    fun isCustomTypeRust(nodeType: NodeType.Parameter): Boolean = nodeType.parameterType.parent!!.languageType[OutputLanguage.Rust]?.let {
        when (val languageType = it) {
            is RustEntry.RustStruct -> languageType.customType
            else -> false
        }
    } ?: false

    fun getFullyQualifiedProtoRustTypeName(baseNode: BaseNode): String {
        val protoEntry = protoEntryOf(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parentOpt = getParentForNode(baseNode)
        val parent = parentOpt!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        when (protoEntry) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                val localTypeName = when (parentIsRoot) {
                    true -> protoModelModule + MOD_SEP + protoEntry.typeName
                    // if the parent isn't the root, we need to resolve the module name for all parents
                    false -> protoRustModuleName(getFullyQualifiedProtoRustTypeName(parent)) + MOD_SEP + protoEntry.typeName
                }

                return localTypeName
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoRustModuleName(getFullyQualifiedProtoRustTypeName(parent)) + MOD_SEP + protoEntry.enumName
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoRustModuleName(getFullyQualifiedProtoRustTypeName(parent)) + MOD_SEP + protoEntry.oneOfName
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // the fully qualified type is the target of the parameter
                return getFullyQualifiedProtoRustTypeName(getNodeForProtoEntry(protoEntry.protoType, tree, protoCustomTypesTree)!!)
            }
        }
    }

    fun getFullyQualifiedProtoTypeName(baseNode: BaseNode): String {
        val protoEntry = protoEntryOf(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parentOpt = getParentForNode(baseNode)
        val parent = parentOpt!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        when (protoEntry) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                val localTypeName = when (parentIsRoot) {
                    true -> (protoEntry.packagePath?.let { "$it." } ?: "") + protoEntry.typeName
                    // if the parent isn't the root, we need to resolve the module name for all parents
                    false -> protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + MOD_SEP + protoEntry.typeName
                }

                return localTypeName
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + MOD_SEP + protoEntry.enumName
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + MOD_SEP + protoEntry.oneOfName
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // the fully qualified type is the target of the parameter
                return getFullyQualifiedProtoTypeName(getNodeForProtoEntry(protoEntry.protoType, tree, protoCustomTypesTree)!!)
            }
        }
    }
}

fun minLengthParameter(parameterNodeType: NodeType.Parameter): Boolean {
        return (
                parameterNodeType.parentNode?.originalXmlNode?.xmlType is XmlType.SimpleContentExtension
                || parameterNodeType.parentNode?.originalXmlNode?.xmlType is XmlType.Restriction
                || parameterNodeType.parentNode?.originalXmlNode?.xmlType is XmlType.List
        ) && (
                (
                    (parameterNodeType.parameterType is NodeType.BuiltinType)
                    && (parameterNodeType.parameterType as NodeType.BuiltinType).origin ==  BuiltinTypes.XSD_STRING.qname
                )
        || parameterNodeType.list)
}

/**
 * determine if this node is extending a string or a list
*/
fun minLengthApplicable(node: BaseNode): Boolean {
    return when (val nt = node.nodeType) {
        is NodeType.StringEnumeration -> false
        is NodeType.Parameter -> minLengthParameter(nt)
        else -> when {
            node.children.size == 1 && node.children[0].nodeType is NodeType.Parameter -> minLengthParameter(node.children[0].nodeType as NodeType.Parameter)
            node.children.size == 2 && node.children[0].nodeType is NodeType.StringEnumeration -> false
            else -> node.children.any { minLengthApplicable(it) }
        }
    }
}

fun findMinLength(node: BaseNode): Int? {
    return if (minLengthApplicable(node)) {
        val paramNode = when (val nt = node.nodeType) {
            is NodeType.Parameter -> nt
            else -> (node.children[0].nodeType as NodeType.Parameter)
        }
        val minLength = paramNode.restriction?.minLength
        minLength ?: node.children.firstNotNullOfOrNull { findMinLength(it) }
    } else {
        null
    }
}