package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.somda.protosdc.mapping.base.KotlinToXmlBaseTypes
import org.somda.protosdc.mapping.base.XmlToKotlinBaseTypes
import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.xmlprocessor.Constants
import javax.xml.namespace.QName

/**
 * Utility functions to create various Kotlin code particles intended to be used by the XML <-> Kotlin mapper.
 *
 * @param kotlinSource Kotlin code generator utility functions.
 */
class KotlinXmlMappingSource(
    private val kotlinSource: KotlinSource,
    private val oneOfClassPrefix: String,
    private val oneOfClassMemberName: String,
    private val enumTypeName: String
) {

    fun kotlinToXmlUnionCase(
        kotlinChoice: String,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.whenEntry(
            condition = kotlinChoice,
            statement = kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$oneOfClassMemberName")
        )
    }

    fun xmlToKotlinFunctionStart(functionName: String, targetType: String, sourceType: String): String {
        return "fun $functionName($SOURCE: $sourceType): $targetType {"
    }

    fun kotlinToXmlFunctionStart(functionName: String, targetType: String, sourceType: String): String {
        return "fun $functionName($SOURCE: $sourceType, $TARGET: $targetType): $targetType {"
    }

    fun returnTarget() = kotlinSource.returnAny(TARGET)

    fun mapFunctionName(functionName: String) = "map_$functionName"

    fun returnInstanceStart(targetType: String) = kotlinSource.returnAny("$targetType(")

    fun returnInstanceEnd() = ")"

    fun xmlToKotlinField(
        kotlinField: String,
        xmlField: String? = null,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = xmlToKotlinMappingCallOrField(mappingFunction, "$SOURCE${xmlField?.let { ".$it" } ?: ""}") + ","
        )
    }

    fun xmlToKotlinList(
        kotlinField: String,
        parameterTypeMappingFunction: String?,
        textNodeMappingFunction: String
    ): String {
        val mapperCall = xmlToKotlinMappingCallOrField(parameterTypeMappingFunction, "it")
        val rhs = """
            ${ListSplit.FUNCTION_NAME}($SOURCE.textContent)
                .map { $textNodeMappingFunction($SOURCE, it) }
                .map { $mapperCall }
                .toList(),
        """.trimIndent()
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = rhs
        )
    }

    fun xmlToKotlinChildNode(
        kotlinField: String,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = xmlToKotlinMappingCallOrField(
                mappingFunction,
                """${NodeListToList.FUNCTION_NAME}($SOURCE.childNodes).firstOrNull() ?: ${TextNodeCreator.FUNCTION_NAME}($SOURCE, "")"""
            ) + ","
        )
    }

    fun xmlToKotlin(
        kotlinField: String,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = xmlToKotlinMappingCallOrField(mappingFunction, "source") + ","
        )
    }

    fun xmlSequenceToKotlinList(
        kotlinField: String,
        xmlField: QName,
        mappingFunction: String? = null
    ): String {
        val mapperCall = xmlToKotlinMappingCallOrField(mappingFunction, "it")
        val rhs = """
            ${NodeListToList.FUNCTION_NAME}($SOURCE.childNodes).asSequence()
                .filter { it.nodeType == ${KotlinType.DOM_NODE}.ELEMENT_NODE }
                .filter { it.namespaceURI == "${xmlField.namespaceURI}" }
                .filter { it.localName == "${xmlField.localPart}" }
                .map { $mapperCall }
                .toList(),
        """.trimIndent()
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = rhs
        )
    }

    fun kotlinListToXmlList(
        kotlinField: String,
        mappingFunction: String? = null
    ): List<CodeLine> {
        return listOf(
            CodeLine("$TARGET.textContent = $SOURCE.$kotlinField.toList().map {"),
            CodeLine(1, """val subNode = $TARGET_OWNER.createTextNode("")"""),
            CodeLine(1, kotlinToXmlMappingCallOrField(mappingFunction, "it", "subNode")),
            CodeLine(1, "subNode.textContent"),
            CodeLine("""}.joinToString(" ")"""),
        )
    }

    fun kotlinListToXmlSequence(
        xmlQName: QName,
        kotlinField: String,
        mappingFunction: String? = null
    ): List<CodeLine> {
        return listOf(
            CodeLine("$SOURCE.$kotlinField.toList().forEach {"),
            CodeLine(
                1,
                """val subNode = $TARGET_OWNER.createElementNS("${xmlQName.namespaceURI}", "${xmlQName.localPart}")"""
            ),
            CodeLine(1, "$TARGET.appendChild(subNode)"),
            CodeLine(1, """$APPLY_PREFIX(subNode, "${xmlQName.namespaceURI}")"""),
            CodeLine(1, kotlinToXmlMappingCallOrField(mappingFunction, "it", "subNode")),
            CodeLine("}")
        )
    }

    fun requiredKotlinFieldToXmlText(
        kotlinField: String,
        mappingFunction: String? = null,
        indentOffset: Int = 0
    ): List<CodeLine> {
        return listOf(
            CodeLine(
                indentOffset,
                """$TARGET_OWNER.createTextNode("").also {"""
            ),
            CodeLine(indentOffset + 1, "$TARGET.appendChild(it)"),
            CodeLine(indentOffset + 1, kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$kotlinField", "it")),
            CodeLine(indentOffset, "}")
        )
    }

    fun requiredKotlinFieldToXmlElement(
        kotlinField: String,
        xmlQName: QName,
        mappingFunction: String? = null,
        indentOffset: Int = 0
    ): List<CodeLine> {
        return listOf(
            CodeLine(
                indentOffset,
                """$TARGET_OWNER.createElementNS("${xmlQName.namespaceURI}", "${xmlQName.localPart}").also {"""
            ),
            CodeLine(indentOffset + 1, "$TARGET.appendChild(it)"),
            CodeLine(1, """$APPLY_PREFIX(it, "${xmlQName.namespaceURI}")"""),
            CodeLine(indentOffset + 1, kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$kotlinField", "it")),
            CodeLine(indentOffset, "}")
        )
    }

    fun optionalKotlinFieldToXmlElement(
        kotlinField: String,
        xmlQName: QName,
        mappingFunction: String? = null
    ): List<CodeLine> {
        return listOf(CodeLine("if ($SOURCE.$kotlinField != null) {")) +
                requiredKotlinFieldToXmlElement("$kotlinField!!", xmlQName, mappingFunction, 1) +
                listOf(CodeLine("}"))
    }

    fun kotlinToXmlOneOfStart() = kotlinSource.returnAny(kotlinSource.whenStart(SOURCE))

    fun xmlToKotlinUnionCase(
        kotlinChoice: String,
        mappingFunction: String? = null
    ): List<CodeLine> {
        return listOf(
            CodeLine("try {"),
            CodeLine(
                1,
                kotlinSource.returnAny(
                    xmlToKotlinMappingCallOrField(
                        mappingFunction,
                        SOURCE
                    ).let { "$kotlinChoice($it)" })
            ),
            CodeLine("}"),
            CodeLine("catch (e: kotlin.Exception) {"),
            CodeLine(1, "// pass through"),
            CodeLine("}")
        )
    }

    fun xmlInheritanceToKotlinStart(
        xmlQName: QName,
        kotlinChoice: String,
        mappingFunction: String? = null
    ): List<CodeLine> {
        val xsiType = Constants.xsiType()
        val mapQName = KotlinSource.qualifiedPathFrom(XmlToKotlinBaseTypes::class.qualifiedName!!, "mapQName")
        return listOf(
            CodeLine(kotlinSource.whenStart("""val typeAttrNode = $SOURCE.attributes.getNamedItemNS("${xsiType.namespaceURI}", "${xsiType.localPart}")""")),
            CodeLine(
                1, kotlinSource.whenEntry(
                    condition = "null",
                    statement = kotlinSource.returnAny(
                        xmlToKotlinMappingCallOrField(
                            mappingFunction,
                            SOURCE
                        ).let { "$kotlinChoice(${kotlinSource.assignment(oneOfClassMemberName, it)})" }
                    )
                )
            ),
            CodeLine(
                1, kotlinSource.whenEntry(
                    condition = "else",
                    statement = kotlinSource.whenStart("val qName = $mapQName(typeAttrNode)")
                )
            )
        )
    }

    fun kotlinToXmlInheritanceStart(): String {
        return kotlinSource.whenStart(SOURCE)
    }

    fun kotlinToXmlInheritanceEntry(
        xmlQName: QName,
        kotlinChoice: String,
        defaultKotlinChoice: String,
        mappingFunction: String? = null
    ): List<CodeLine> {
        val xsiQName = Constants.xsiType()
        val mapQName = KotlinSource.qualifiedPathFrom(KotlinToXmlBaseTypes::class.qualifiedName!!, "mapQName")
        val xsiType = when (kotlinChoice == defaultKotlinChoice) {
            true -> listOf()
            false -> listOf(
                CodeLine(
                    1,
                    """$TARGET_OWNER.createAttributeNS("${xsiQName.namespaceURI}", "${xsiQName.localPart}").also {"""
                ),
                CodeLine(2, "$TARGET.attributes.setNamedItemNS(it)"),
                CodeLine(2, """$APPLY_PREFIX(it, "${xsiQName.namespaceURI}")"""),
                CodeLine(2, "$mapQName(${qNameOf(xmlQName)}, it)"),
                CodeLine(1, kotlinSource.blockEnd())
            )
        }

        return listOf(CodeLine(kotlinSource.whenEntry(condition = "is $kotlinChoice", statement = "{"))) +
                xsiType +
                listOf(
                    CodeLine(
                        1,
                        kotlinSource.returnAny(
                            kotlinToXmlMappingCallOrField(
                                mappingFunction,
                                "$SOURCE.$oneOfClassMemberName"
                            )
                        )
                    ),
                    CodeLine(kotlinSource.blockEnd())
                )
    }

    fun qNameOf(qName: QName): String {
        return """${KotlinType.QNAME}("${qName.namespaceURI}", "${qName.localPart}")"""
    }

    fun xmlInheritanceToKotlinEntry(
        xmlQName: QName,
        kotlinChoice: String,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.whenEntry(
            condition = qNameOf(xmlQName),
            statement = kotlinSource.returnAny(
                xmlToKotlinMappingCallOrField(
                    mappingFunction,
                    SOURCE
                ).let { "$kotlinChoice(${kotlinSource.assignment(oneOfClassMemberName, it)})" }
            )
        )
    }

    fun xmlElementToKotlin(kotlinField: String, namespace: String, localName: String): List<CodeLine> = listOf(
        CodeLine(
            kotlinSource.assignment(
                lhs = kotlinField,
                rhs = "when (val elem = ${NodeListToList.FUNCTION_NAME}($SOURCE.childNodes).asSequence()"
            )
        ),
        CodeLine(1, """.filter { it.nodeType == ${KotlinType.DOM_NODE}.ELEMENT_NODE }"""),
        CodeLine(1, """.filter { it.namespaceURI == "$namespace" }"""),
        CodeLine(1, """.filter { it.localName == "$localName" }"""),
        CodeLine(1, ".firstOrNull()) {")
    )

    fun optionalXmlElementToKotlinNull() = kotlinSource.whenEntry(
        condition = "null",
        statement = "null"
    )

    fun requiredXmlElementToKotlinNull(nodeName: String) = kotlinSource.whenEntry(
        condition = "null",
        statement = """throw Exception("Element '$nodeName' required, but missing")"""
    )

    fun xmlElementToKotlinElse(
        mappingFunction: String?
    ) = kotlinSource.whenEntry(
        condition = "else",
        statement = xmlToKotlinMappingCallOrField(mappingFunction, "elem")
    )

    fun xmlTextToKotlinElse(
        mappingFunction: String?
    ): List<CodeLine> {
        return listOf(
            CodeLine(
                kotlinSource.whenEntry(
                    condition = "else",
                    statement = """(${NodeListToList.FUNCTION_NAME}(elem.childNodes).firstOrNull() ?: ${TextNodeCreator.FUNCTION_NAME}(elem, "")).let {"""
                )
            ),
            CodeLine(
                1,
                xmlToKotlinMappingCallOrField(mappingFunction, "it"),
            ),
            CodeLine(kotlinSource.blockEnd())
        )
    }

    fun xmlAttributeToKotlin(kotlinField: String, attributeName: String) = kotlinSource.assignment(
        lhs = kotlinField,
        rhs = kotlinSource.whenStart("""val attr = $SOURCE.attributes.getNamedItem("$attributeName")""")
    )

    fun xmlTextToKotlin(kotlinField: String, mappingFunction: String?): List<CodeLine> = listOf(
        CodeLine(
            kotlinSource.assignment(
                lhs = kotlinField,
                rhs = """(${NodeListToList.FUNCTION_NAME}($SOURCE.childNodes).firstOrNull() ?: $SOURCE).let {"""
            )
        ),
        CodeLine(
            1,
            xmlToKotlinMappingCallOrField(mappingFunction, "it"),
        ),
        CodeLine(kotlinSource.blockEnd() + ",")
    )

    fun kotlinToXmlAttribute(
        kotlinField: String, xmlName: QName, mappingFunction: String?,
        indentOffset: Int = 0
    ): List<CodeLine> {
        val attrNamespace = if (xmlName.namespaceURI == "") {
            "null"
        } else {
            """"${xmlName.namespaceURI}""""
        }

        return listOf(
            CodeLine(
                indentOffset,
                """$TARGET_OWNER.createAttributeNS($attrNamespace, "${xmlName.localPart}").also {"""
            ),
            CodeLine(indentOffset + 1, "$TARGET.attributes.setNamedItemNS(it)"),
            CodeLine(indentOffset + 1, """$APPLY_PREFIX(it, $attrNamespace)"""),
            CodeLine(
                indentOffset + 1,
                kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$kotlinField", "it")
            ),
            CodeLine(indentOffset, "}")
        )
    }

    fun optionalKotlinToXmlAttribute(kotlinField: String, xmlName: QName, mappingFunction: String?): List<CodeLine> {
        return listOf(CodeLine("if ($SOURCE.$kotlinField != null) {")) +
                kotlinToXmlAttribute("$kotlinField!!", xmlName, mappingFunction, 1) +
                CodeLine("}")
    }

    fun kotlinToXmlAttributeGroup(mappingFunction: String?, kotlinFieldName: String) =
        kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$kotlinFieldName")


    fun kotlinToXml(mappingFunction: String?, kotlinFieldName: String) =
        kotlinToXmlMappingCallOrField(mappingFunction, "$SOURCE.$kotlinFieldName")

    fun xmlAttributeToKotlinElse(
        attributeName: String,
        mappingFunction: String?
    ) = kotlinSource.whenEntry(
        condition = "else",
        statement = xmlToKotlinMappingCallOrField(
            mappingFunction,
            "attr"
        )
    )

    fun optionalXmlAttributeToKotlinNull() = kotlinSource.whenEntry(
        condition = "null",
        statement = "null"
    )

    fun requiredXmlAttributeToKotlinNull(attributeName: String) = kotlinSource.whenEntry(
        condition = "null",
        statement = """throw Exception("Attribute '$attributeName' required, but missing")"""
    )

    fun xmlAttributeToKotlinEnd() = kotlinSource.blockEnd() + ","

    fun xmlToKotlinEnumStart() = kotlinSource.returnAny(kotlinSource.whenStart("$SOURCE.textContent"))

    fun kotlinToXmlEnumStart() = kotlinSource.returnAny(kotlinSource.whenStart(SOURCE))

    fun xmlToKotlinEnumCase(
        sourceValue: String,
        targetType: String,
        targetValue: String
    ) = kotlinSource.whenEntry(
        condition = """"$sourceValue"""",
        statement = "$targetType.$targetValue"
    )

    fun kotlinToXmlEnumCase(
        sourceType: String,
        sourceValue: String,
        targetValue: String
    ) = kotlinSource.whenEntry(
        condition = "$sourceType.$sourceValue",
        statement = """$TARGET.also { it.textContent = "$targetValue" }"""
    )

    fun xmlToKotlinEnumElse() = kotlinSource.whenEntry(
        "else",
        """throw Exception("Unknown enum value $$SOURCE")"""
    )

    fun xmlToKotlinEnumEnd() = kotlinSource.blockEnd()

    fun xmlToKotlinEnumType(
        targetType: String,
        mappingFunction: String?
    ): String {
        return kotlinSource.assignment(
            lhs = targetType,
            rhs = xmlToKotlinMappingCallOrField(mappingFunction, SOURCE) + ","
        )
    }

    fun oneOfKotlinEntryName(name: String) = "$oneOfClassPrefix$name"

    companion object {
        private const val APPLY_PREFIX = NamespaceMapping.FUNCTION_NAME_APPLY_PREFIX
        const val SOURCE = "source"
        const val TARGET = "target"
        const val TARGET_OWNER = "$TARGET.ownerDocument"

        /**
         * Creates a fully qualified name to be used in a mapper function name by replacing dot with underscores.
         */
        fun functionNameOfPackage(packageName: String) = packageName.replace(".", "_")

        private fun kotlinToXmlMappingCallOrField(
            funcName: String?,
            funcParam: String,
            target: String = TARGET
        ): String {
            check(funcName != null) { "No mapper function defined" }
            return "$funcName($funcParam, $target)"
        }

        private fun xmlToKotlinMappingCallOrField(funcName: String?, funcParam: String): String {
            check(funcName != null) { "No mapper function defined" }
            return "$funcName($funcParam)"
        }
    }
}