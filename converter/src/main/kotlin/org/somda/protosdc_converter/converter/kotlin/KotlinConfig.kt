package org.somda.protosdc_converter.converter.kotlin

@kotlinx.serialization.Serializable
data class KotlinConfig(
    val kotlinPackage: String,
    val outputFolders: List<String> = emptyList(),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
    val indentationSize: Long = 4,
    val oneOfClassPrefix: String = "Choice",
    val oneOfClassMemberName: String = "value",
    val enumTypeName: String = "EnumType"
)