package org.somda.protosdc_converter.converter.kotlinmapper.xml

@kotlinx.serialization.Serializable
data class KotlinXmlMappingConfig(
    val mappingPackage: String,
    val outputFolders: List<String> = emptyList(),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
    val kotlinToXmlClassName: String = "KotlinToXml",
    val xmlToKotlinClassName: String = "XmlToKotlin"
)