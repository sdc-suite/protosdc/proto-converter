package org.somda.protosdc_converter.converter.kotlinmapper.proto

@kotlinx.serialization.Serializable
data class KotlinProtoExtensionsConfig(
    val mappingPackage: String,
    val outputFolders: List<String> = emptyList(),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
    val outputClassName: String = "ProtoExtensionsMapper",
    val fullyQualifiedInterfaceName: String? = null,
)