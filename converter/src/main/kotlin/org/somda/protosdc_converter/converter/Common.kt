package org.somda.protosdc_converter.converter

import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import java.io.File
import java.nio.file.Paths
import java.util.*

const val PARTICIPANT_MODEL_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant"


private fun findIllegalFolderContents(folder: File, extension: String): List<String> {
    return folder.walk().filter {
        when {
            it.isFile -> it.extension != extension
            else -> false
        }
    }.map { it.absolutePath }.toList()
}

/**
 * Requires a folder to only contain files with a given extension.
 *
 * @param folder folder to search through
 * @param extension to search for
 * @throws IllegalArgumentException
 */
fun requireLegalFolderContents(folder: File, extension: String) {
    val illegalContent = findIllegalFolderContents(folder, extension)
    require(illegalContent.isEmpty()) {
        "${folder.absolutePath} contains files which are not .$extension files. Forbidden files $illegalContent"
    }
}

data class FolderInfo(val folder: File, val createHierarchy: Boolean = false)

fun determineOutputFolders(
    baseFolder: String,
    outputFolders: List<String>,
    outputFoldersNoHierarchy: List<String>,
    fileExtension: String,
): List<FolderInfo> {
    // tag output folders that are not supposed to have the hierarchy with a bool
    val mappedFolderNames = outputFolders.asSequence().map { it to true } +
            outputFoldersNoHierarchy.asSequence().map { it to false }

    return mappedFolderNames.map { (folder, createHierarchy) ->
        val outputFolder = File(baseFolder, folder)
        requireLegalFolderContents(outputFolder, fileExtension)
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()
        FolderInfo(outputFolder, createHierarchy)
    }.toList()
}

fun prepareOutputFile(
    outputFolders: List<FolderInfo>,
    fileName: String,
    segments: Array<String>,
    disableHierarchy: Boolean = false,
): List<File> {
    val filesToWrite = outputFolders.map { (folder, createHierarchy) ->
        var directory = folder
        if (createHierarchy && !disableHierarchy) {
            directory = File(Paths.get(folder.absolutePath, *segments).toUri())
            if (!directory.exists()) {
                directory.mkdirs()
            }
        }
        File(directory, fileName)
    }
    return filesToWrite
}

fun protoEntryOf(node: BaseNode): Proto3Generator.ProtoEntry {
    return node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
}

fun BaseNode.protoLanguageType() = protoEntryOf(this)

fun getNodeForProtoEntry(
    protoEntry: Proto3Generator.ProtoEntry,
    tree: BaseNode,
    protoCustomTypesTree: BaseNode,
): BaseNode? {
    tree.traversePreOrder().forEach { child ->
        if (child.nodeType !is NodeType.Root) {
            val nodeProtoEntry = protoEntryOf(child)
            if (protoEntry === nodeProtoEntry) {
                return child
            }
        }
    }
    protoCustomTypesTree.traversePreOrder().forEach { child ->
        if (child.nodeType !is NodeType.Root) {
            val nodeProtoEntry = protoEntryOf(child)
            if (protoEntry === nodeProtoEntry) {
                return child
            }
        }
    }
    return null
}

fun String.firstToUpper() =
    this.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

fun String.firstToLower() =
    this.replaceFirstChar { it.lowercase(Locale.getDefault()) }