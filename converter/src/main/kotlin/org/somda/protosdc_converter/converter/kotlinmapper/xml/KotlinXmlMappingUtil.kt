package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.somda.protosdc_converter.converter.kotlin.KotlinEntry
import org.somda.protosdc_converter.converter.kotlinmapper.MapperMetadataRegistry
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.EpisodeData
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

const val KOTLIN_XML_MAPPER_PLUGIN_IDENTIFIER_V1 = "kotlinxmlmapperv1"
const val KOTLIN_XML_MAPPER_TOML_TABLE_NAME = "KotlinXmlMapping"

/**
 * Gets the episode data for the Kotlin Mapper plugin.
 */
fun NodeType.Message.kotlinXmlMapperEpisodeMapperData(): EpisodeData.Mapper? {
    return episodePluginData[KOTLIN_XML_MAPPER_PLUGIN_IDENTIFIER_V1] as? EpisodeData.Mapper
}

/**
 * Marks a cluster as handled for the KotlinToProto mapper.
 */
fun BaseNode.kotlinToXmlSetClusterHandled() {
    clusterHandled[OutputLanguage.KotlinToXmlMapper] = true
}

/**
 * Checks if a cluster was handled already for the KotlinToProto mapper.
 */
fun BaseNode.kotlinToXmlClusterHandled() = clusterHandled[OutputLanguage.KotlinToXmlMapper] ?: false

/**
 * Marks a cluster as handled for the ProtoToKotlin mapper.
 */
fun BaseNode.xmlToKotlinSetClusterHandled() {
    clusterHandled[OutputLanguage.XmlToKotlinMapper] = true
}

/**
 * Checks if a cluster was handled already for the ProtoToKotlin mapper.
 */
fun BaseNode.xmlToKotlinClusterHandled() = clusterHandled[OutputLanguage.XmlToKotlinMapper] ?: false

/**
 * Interface to express functionality relevant to both mapping directions.
 */
interface KotlinXmlMapperUtil {
    fun fullyQualifiedXmlNameFor(baseNode: BaseNode): String
    fun fullyQualifiedKotlinNameFor(baseNode: BaseNode): String
    fun getKotlinFieldName(kotlin: KotlinEntry): String
    fun toXmlEnumName(value: String): String

    val xmlToKotlinMapperMap: MapperMetadataRegistry

    val kotlinToXmlMapperMap: MapperMetadataRegistry
}