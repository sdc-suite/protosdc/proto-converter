package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.primitiveParserCall
import org.somda.protosdc_converter.converter.rust.simpleParserCall
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import javax.xml.namespace.QName

abstract class AttributeInfo {
    abstract val attributeQName: QName
    abstract val attributeType: BaseNode

    // original parameter node referencing this attribute, used to reconstruct type
    abstract val attributeOriginNode: BaseNode

    abstract val isOptional: Boolean
    abstract val isList: Boolean
    abstract val attributeAccessPath: String
}

data class WriterAttributeInfo(
    override val attributeQName: QName,
    override val attributeType: BaseNode,

    // original parameter node referencing this attribute, used to reconstruct type
    override val attributeOriginNode: BaseNode,

    override val isOptional: Boolean,
    override val isList: Boolean,
    override val attributeAccessPath: String = "",

    val attributeTypePrimitiveWriter: RustWriter?,
    val attributeTypeSimpleWriter: RustWriter?,
): AttributeInfo() {
    init {
        check((attributeTypePrimitiveWriter != null).xor(attributeTypeSimpleWriter != null))
    }
}

data class ParserAttributeInfo(
    override val attributeQName: QName,
    override val attributeType: BaseNode,

    // original parameter node referencing this attribute, used to reconstruct type
    override val attributeOriginNode: BaseNode,

    override val isOptional: Boolean,
    override val isList: Boolean,
    override val attributeAccessPath: String = "",

    val attributeTypePrimitiveParser: String?,
    val attributeTypeSimpleParser: String?,
    val rustTypeName: String,
): AttributeInfo() {
    init {
        check(attributeTypeSimpleParser != null || attributeTypePrimitiveParser != null) {
            "No parser found for $attributeQName of type $attributeType"
        }
        check(!(attributeTypeSimpleParser != null && attributeTypePrimitiveParser != null))
    }

    fun parserCall(variable: String) = when {
        attributeTypePrimitiveParser != null -> primitiveParserCall(attributeTypePrimitiveParser, variable)
        else -> simpleParserCall(rustTypeName, variable)
    }
}