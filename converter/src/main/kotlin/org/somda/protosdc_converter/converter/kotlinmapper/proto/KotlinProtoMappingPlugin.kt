package org.somda.protosdc_converter.converter.kotlinmapper.proto

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.*
import org.somda.protosdc_converter.converter.Proto3Generator.Companion.camelCaseToConstant
import org.somda.protosdc_converter.converter.kotlin.*
import org.somda.protosdc_converter.xmlprocessor.*
import org.somda.protosdc.mapping.base.KotlinToProtoBaseTypes
import org.somda.protosdc.mapping.base.ProtoToKotlinBaseTypes
import org.somda.protosdc_converter.converter.kotlinmapper.FullMappingDefinition
import org.somda.protosdc_converter.converter.kotlinmapper.MapperMetadataRegistry
import org.somda.protosdc_converter.converter.kotlinmapper.mapperFunctionName
import java.io.ByteArrayOutputStream
import java.io.PrintWriter
import kotlin.reflect.KFunction
import kotlin.reflect.full.declaredFunctions

/**
 * Plugin to generate mappers Proto <-> Kotlin.
 */
class KotlinProtoMappingPlugin(
    private val tree: BaseNode,
    private val protoCustomTypesTree: BaseNode,
    converterConfig: ConverterConfig,
    kotlinConfig: KotlinConfig,
    private val kotlinProtoMappingConfig: KotlinProtoMappingConfig,
    private val customMappers: List<ProtoMapping>,
    private val generateCustomTypes: Boolean = true,
) : BaseNodePlugin, KotlinProtoMapperUtil {

    private val mergedTree = BaseNode("MergedTree", mutableListOf(tree, protoCustomTypesTree))

    private val kotlinSource = KotlinSource(
        kotlinConfig.indentationSize.toInt(),
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val kotlinMappingSource = KotlinProtoMappingSource(
        kotlinSource,
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val outputFolders = setupOutputFolders(
        converterConfig.outputFolder,
        kotlinProtoMappingConfig.outputFolders,
        kotlinProtoMappingConfig.outputFoldersNoHierarchy
    ) { "Output folders are missing for Kotlin Mapper generator" }

    override val protoToKotlinMapperMap = MapperMetadataRegistry()
    override val kotlinToProtoMapperMap = MapperMetadataRegistry()

    /**
     * Creates a fully qualified name for a base node.
     *
     * This method throws if performed on a root node.
     */
    override fun fullyQualifiedProtoNameFor(baseNode: BaseNode): String {
        val protoEntry = baseNode.protoLanguageType()
        val parent = parentForNode(baseNode)!!

        return when (protoEntry) {
            is Proto3Generator.ProtoEntry.ProtoType -> if (parent.nodeType is NodeType.Root) {
                // if this is a node under the parent, this is correct
                // abort recursion from here
                protoEntry.fullyQualifiedName()
            } else {
                // recursively apply on parents and join to package with the proto entry type name
                KotlinSource.qualifiedPathFrom(fullyQualifiedProtoNameFor(parent), protoEntry.typeName)
            }

            is Proto3Generator.ProtoEntry.ProtoOneOf ->
                // these literally only exist in the context of their parent, we need to resolve it
                KotlinSource.qualifiedPathFrom(fullyQualifiedProtoNameFor(parent), protoEntry.oneOfName)

            is Proto3Generator.ProtoEntry.ProtoEnum ->
                // these literally only exist in the context of their parent, we need to resolve it
                KotlinSource.qualifiedPathFrom(fullyQualifiedProtoNameFor(parent), protoEntry.enumName)

            is Proto3Generator.ProtoEntry.ProtoParameter ->
                fullyQualifiedProtoNameFor(nodeForProtoEntry(protoEntry.protoType)!!)
        }
    }

    override fun fullyQualifiedKotlinNameFor(baseNode: BaseNode): String {
        val kotlinEntry = baseNode.kotlinLanguageType()
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parent = parentForNode(baseNode)!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        val parentIsSkip = when (parentIsRoot) {
            false -> when (val parentLanguageType = parent.kotlinLanguageType()) {
                is KotlinEntry.DataClass -> parentLanguageType.skipType
                else -> false
            }

            true -> false
        }

        when (kotlinEntry) {
            is KotlinEntry.DataClass -> {
                // if this is a node under the parent, this is correct
                if (parentIsRoot) {
                    // distinguish between builtin types (which implies "kotlin." prefix) and other types
                    return kotlinEntry.packagePath?.let {
                        kotlinEntry.importPath()
                    } ?: KotlinSource.qualifiedPathFrom(KOTLIN_PREFIX, kotlinEntry.importPath())
                }
                // resolve the parents fully qualified name and return that with the type name appended
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.typeName)
            }

            is KotlinEntry.KotlinOneOf -> {
                // if the parent is skipped, we are the fully resolved path, else we're just nested
                if (parentIsSkip) {
                    // if the OneOf is unwrapped, it has the exact same name as the parent
                    return fullyQualifiedKotlinNameFor(parent)
                }
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.oneOfName)
            }

            is KotlinEntry.KotlinOneOfParameter -> {
                // fully qualified parent is the suffix here
                val kotlinTypeNode = nodeForKotlinEntry(kotlinEntry.kotlinType)
                return fullyQualifiedKotlinNameFor(kotlinTypeNode!!)
            }

            is KotlinEntry.KotlinParameter -> {
                val kotlinTypeNode = nodeForKotlinEntry(kotlinEntry.kotlinType)
                return fullyQualifiedKotlinNameFor(kotlinTypeNode!!)
            }

            is KotlinEntry.KotlinStringEnumeration -> {
                // fully qualified parent is the suffix here
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.enumName)
            }
        }
    }

    override fun protoToKotlinType(proto: String): String {
        return PROTO_COMPILED_TO_KOTLIN_MAP[proto] ?: run {
            when (proto.startsWith(GOOGLE_PROTO_PACKAGE)) {
                true -> GOOGLE_JAVA_PACKAGE + proto.removePrefix(GOOGLE_PROTO_PACKAGE)
                false -> proto
            }
        }
    }

    override fun getProtoFieldName(proto: Proto3Generator.ProtoEntry, pascalCase: Boolean): String {
        return when (proto) {
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // snake to camel case the parameter
                val name = proto.parameterName.split('_')
                    .joinToString("", transform = String::firstToUpper)
                when (pascalCase) {
                    true -> name
                    false -> KotlinSource.toParameterName(name)
                }

            }

            else -> throw Exception("Handle $proto")
        }
    }

    override fun getKotlinFieldName(kotlin: KotlinEntry): String {
        return when (kotlin) {
            is KotlinEntry.KotlinParameter -> {
                kotlin.parameterName
            }

            else -> throw Exception("Handle $kotlin")
        }
    }

    override fun toProtoEnumName(value: String): String {
        return value.camelCaseToConstant()
    }

    private val kotlinToProtoImpl = KotlinToProtoImpl(
        kotlinSource,
        kotlinMappingSource,
        this,
        kotlinConfig.oneOfClassPrefix
    )
    private val protoToKotlinImpl = ProtoToKotlinImpl(
        kotlinSource,
        kotlinMappingSource,
        this
    )

    init {
        assignMapperDataFromBuiltInMappers()
        logger.info { "Builtin mappers registered" }
        assignMapperDataFromCustomMappers()
        logger.info { "Custom mappers registered" }
    }

    override fun run() {
        logger.info("Creating Kotlin <-> Proto mapping")

        assignMapperDataFromEpisodes()

        writeMapper(
            kotlinProtoMappingConfig.protoToKotlinClassName,
            customMappers.map { it.fromProto() }) { node, level, target ->
            protoToKotlinImpl.processNode(node, target, level)
        }

        writeMapper(
            kotlinProtoMappingConfig.kotlinToProtoClassName,
            customMappers.map { it.toProto() }) { node, level, target ->
            kotlinToProtoImpl.processNode(node, target, level)
        }
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        val qualifiedProtoTypeName = protoToKotlinType(fullyQualifiedProtoNameFor(node))
        val qualifiedKotlinTypeName = fullyQualifiedKotlinNameFor(node)

        // store kotlin as source and proto as target
        return EpisodeData.Mapper(
            packageName = kotlinProtoMappingConfig.mappingPackage,
            sourceToTargetFunction = kotlinToProtoMapperMap[qualifiedKotlinTypeName, qualifiedProtoTypeName]!!,
            targetToSourceFunction = protoToKotlinMapperMap[qualifiedProtoTypeName, qualifiedKotlinTypeName]!!,
            sourceTypeName = qualifiedKotlinTypeName,
            targetTypeName = qualifiedProtoTypeName,
            sourceStructName = kotlinProtoMappingConfig.kotlinToProtoClassName,
            targetStructName = kotlinProtoMappingConfig.protoToKotlinClassName
        )
    }

    override fun pluginIdentifier(): String {
        return KOTLIN_PROTO_MAPPER_PLUGIN_IDENTIFIER_V2
    }

    private fun writeMapper(
        outputClassName: String,
        customMappers: List<FullMappingDefinition>,
        processNode: (node: BaseNode, level: Int, target: PrintWriter) -> Unit
    ) {
        // determine directory structure required based on package
        val filesToWrite = determineFilesToWrite(
            outputFolders,
            kotlinProtoMappingConfig.mappingPackage,
            appendKotlinFileExtension(outputClassName)
        )

        val byteStream = ByteArrayOutputStream()
        PrintWriter(byteStream).use { printWriter ->
            // print package declaration
            printWriter.println(kotlinSource.packageDeclaration(kotlinProtoMappingConfig.mappingPackage))
            printWriter.println()

            // no imports needed, every type is rendered fully qualified

            // begin object class, all mapper functions are static
            printWriter.println(kotlinSource.objectStart(outputClassName))

            customMappers.forEach {
                val result = kotlinSource.indentLines(1) { it.code }
                printWriter.println(result)
            }

            tree.children.filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.skipGenerating
                    else -> true
                }
            }.filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.customType || generateCustomTypes
                    else -> true
                }
            }.forEach { node ->
                processNode(node, 1, printWriter)
            }

            printWriter.println(kotlinSource.blockEnd())
        }

        writeByteStreamToFile(byteStream, filesToWrite)
    }

    private fun assignMapperDataFromBuiltInMappers() {
        val protoToKotlinFunctions = ProtoToKotlinBaseTypes::class.declaredFunctions
        val protoToKotlinFullyQualifiedName = ProtoToKotlinBaseTypes::class.qualifiedName!!
        protoToKotlinFunctions.forEach { func ->
            registerSignature(protoToKotlinFullyQualifiedName, func, protoToKotlinMapperMap)
        }

        val kotlinToProtoFunctions = KotlinToProtoBaseTypes::class.declaredFunctions
        val kotlinToProtoFullyQualifiedName = KotlinToProtoBaseTypes::class.qualifiedName!!
        kotlinToProtoFunctions.forEach { func ->
            registerSignature(kotlinToProtoFullyQualifiedName, func, kotlinToProtoMapperMap)
        }
    }

    private fun assignMapperDataFromCustomMappers() {
        customMappers.forEach {
            it.fromProto().also { mapping ->
                protoToKotlinMapperMap[mapping.sourceType, mapping.targetType] = mapping.mapperFunctionName
            }
            it.toProto().also { mapping ->
                kotlinToProtoMapperMap[mapping.sourceType, mapping.targetType] = mapping.mapperFunctionName
            }
        }
    }

    private fun assignMapperDataFromEpisodes() {
        // attach all mappers stored through an episode
        tree.traversePreOrder().mapNotNull {
            it.nodeType?.asMessage()?.kotlinProtoMapperEpisodeMapperData()
        }.forEach {
            kotlinToProtoMapperMap[it.sourceTypeName, it.targetTypeName] =
                mapperFunctionName(it.packageName, it.sourceStructName, it.sourceToTargetFunction)
            protoToKotlinMapperMap[it.targetTypeName, it.sourceTypeName] =
                mapperFunctionName(it.packageName, it.targetStructName, it.targetToSourceFunction)
        }
    }

    /**
     * Looks up the parent of a given node.
     *
     * Parent search includes the children of the main base node tree as well as the proto custom types tree,
     * which includes proto messages to express optional properties.
     */
    private fun parentForNode(node: BaseNode): BaseNode? {
        // if direct child to root, quicker result
        if (tree.children.contains(node)) {
            return tree
        }

        mergedTree.traversePreOrder().forEach { parent ->
            parent.children.forEach { child ->
                if (node === child) {
                    return parent
                }
            }
        }

        return null
    }

    /**
     * Looks up the base node for a given [KotlinEntry].
     */
    private fun nodeForKotlinEntry(kotlinEntry: KotlinEntry): BaseNode? {
        for (node in tree.traversePreOrder()) {
            if (kotlinEntry.entryId == node.kotlinLanguageTypeOrNull()?.entryId) {
                return node
            }
        }

        return null
    }

    /**
     * Looks up the base node for a given [KotlinEntry].
     */
    private fun nodeForProtoEntry(protoEntry: Proto3Generator.ProtoEntry): BaseNode? {
        return getNodeForProtoEntry(protoEntry, tree, protoCustomTypesTree)
    }

    companion object : Logging {
        private const val GOOGLE_JAVA_PACKAGE = "com.google.protobuf"
        private const val GOOGLE_PROTO_PACKAGE = Proto3Generator.GOOGLE_PACKAGE_SUFFIX
        private const val KOTLIN_PREFIX = "kotlin"

        private val PROTO_COMPILED_TO_KOTLIN_MAP = mapOf(
            "string" to "kotlin.String",
            "int64" to "kotlin.Long",
            "uint64" to "kotlin.Long",
            "bool" to "kotlin.Boolean",
            "uint32" to "kotlin.Int",
            "int32" to "kotlin.Int",
        )

        fun parseConfig(config: String): KotlinProtoMappingConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, KOTLIN_PROTO_MAPPER_TOML_TABLE_NAME)
        }

        private fun registerSignature(
            fullyQualifiedName: String,
            func: KFunction<*>,
            mapperMap: MapperMetadataRegistry
        ) {
            // why can't I access the name in a sane manner somewhere? Why the is toString needed? This is stupid.
            val functionName = func.name
            check(func.parameters.size == 2) { "Mappers may only have one parameter! $functionName has ${func.parameters.size}" }
            val actualParam = func.parameters[1]
            val functionParameterName = actualParam.name

            val functionParameterType = actualParam.type.toString()
            val functionReturnType = func.returnType.toString()

            logger.info { "$fullyQualifiedName::$functionName($functionParameterName: $functionParameterType): $functionReturnType" }

            val fullyQualifiedCall = "$fullyQualifiedName.$functionName"

            mapperMap[functionParameterType, functionReturnType] = fullyQualifiedCall
        }
    }
}