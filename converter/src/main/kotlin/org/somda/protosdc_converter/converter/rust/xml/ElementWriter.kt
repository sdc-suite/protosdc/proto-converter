package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlWriterConfig
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser
import org.somda.protosdc_converter.converter.rust.RustQuickXmlWriter.Companion.logger
import org.somda.protosdc_converter.converter.rust.SELF
import org.somda.protosdc_converter.converter.rust.TAG_NAME
import org.somda.protosdc_converter.converter.rust.UNIT
import org.somda.protosdc_converter.converter.rust.WRITER
import org.somda.protosdc_converter.converter.rust.XSI_TYPE_ARG
import org.somda.protosdc_converter.converter.rust.accessAncestor
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.callElementWriter
import org.somda.protosdc_converter.converter.rust.callPrimitiveWriter
import org.somda.protosdc_converter.converter.rust.callSimpleWriter
import org.somda.protosdc_converter.converter.rust.camelToSnakeCase
import org.somda.protosdc_converter.converter.rust.elementWriterSignature
import org.somda.protosdc_converter.converter.rust.isChoiceContainer
import org.somda.protosdc_converter.converter.rust.some
import org.somda.protosdc_converter.converter.rust.validateName
import org.somda.protosdc_converter.converter.rust.xml.OneOfContentWriter.Companion.generateOneOfMiddle
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class ElementWriter(val writerName: String, val dataType: BaseNode, val element: WriterElementInfo) : WriterTracker {
    override fun generateWriter(
        config: QuickXmlWriterConfig,
        primitiveWriters: MutableMap<BaseNode, RustWriter>,
        simpleWriters: MutableMap<BaseNode, RustWriter>,
        complexWriters: MutableMap<BaseNode, RustWriter>
    ): String {
        val targetType = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct
        val targetTypeName = targetType.fullyQualifiedName(config.modelRustModuleSubstitute)

        val baseIndent = 2

        val functionStart: String = elementWriterSignature(targetTypeName)
        val functionEnd = "\n${INDENT.repeat(baseIndent)}Ok($UNIT)\n${INDENT.repeat(baseIndent - 1)}}\n}"

        // find all types we're inheriting from
        val ancestors = (element.elementNode.nodeType as NodeType.Message).extensionBaseNode?.let {
            RustQuickXmlParser.collectAncestors(
                it
            )
        }
            ?: emptyList()
        logger.info { "Ancestors for $element are $ancestors" }


        // Attribute, variable name
        val processedAttributes: Map<WriterAttributeInfo, String> = element.attributes.associateWith {
            validateName(it.attributeQName.localPart.camelToSnakeCase() + ElementParser.ATTRIBUTE_VARIABLE_SUFFIX)
        }

        val targetTypeNamespace = dataType.originalXmlNode?.qName?.namespaceURI
        val targetTypeLocalPart = dataType.originalXmlNode?.qName?.localPart

        val xsiBody = when {
            targetTypeNamespace != null -> "$WRITER.add_attribute_qname(${some("\"http://www.w3.org/2001/XMLSchema-instance\"")}, \"type\", ${
                some(
                    "\"$targetTypeNamespace\""
                )
            }, \"${targetTypeLocalPart!!}\")?;"

            else -> "panic!(\"cannot provide xsi:type for $targetTypeName\");"
        }

        val xsiTypeAttr = """
${INDENT.repeat(baseIndent)}if $XSI_TYPE_ARG {
${INDENT.repeat(baseIndent + 1)}$xsiBody
${INDENT.repeat(baseIndent)}}
"""

        val attributeParsers = processedAttributes.map { (attribute, fieldName) ->

            var fieldAccess = when (attribute.attributeAccessPath.isBlank()) {
                false -> attribute.attributeAccessPath
                true -> ".$fieldName"
            }

            if (!fieldAccess.startsWith(".")) {
                fieldAccess = ".$fieldAccess"
            }

            val field = "$SELF${fieldAccess}"


            val writerCall: (String, Boolean) -> String = when {
                attribute.attributeTypePrimitiveWriter != null -> { variable, ref ->
                    callPrimitiveWriter(
                        config,
                        attribute.attributeTypePrimitiveWriter,
                        if (ref) "&$variable" else variable
                    )
                }

                else -> { variable, _ -> callSimpleWriter(variable) }
            }

            fun attributeWriterCall(writerCall: String, baseLevel: Int = 0) =
                when (attribute.attributeQName.namespaceURI.isBlank()) {
                    true -> """${INDENT.repeat(baseLevel)}{
${INDENT.repeat(baseLevel + 1)}let data = $writerCall;
${INDENT.repeat(baseLevel + 1)}$WRITER.add_attribute(("${attribute.attributeQName.localPart}".as_bytes(), data.as_slice()))?;
${INDENT.repeat(baseLevel)}}"""

                    false -> """${INDENT.repeat(baseLevel)}let data_internal = $writerCall;
${INDENT.repeat(baseLevel)}let data = std::str::from_utf8(&data_internal)?;
${INDENT.repeat(baseLevel)}$WRITER.add_attribute_qname(Some("${attribute.attributeQName.namespaceURI}"), "${attribute.attributeQName.localPart}", None, &data)?;"""
                }

            when {
                attribute.isList -> {
                    """${INDENT.repeat(2)}if !$field.empty() {
${INDENT.repeat(3)}for x in $field {
${attributeWriterCall("${writerCall("x", true)}?", 4)}
${INDENT.repeat(3)}}
${INDENT.repeat(2)}}
"""
                }

                !attribute.isOptional -> {
                    attributeWriterCall("${writerCall(field, true)}?", 2)
                }

                else -> {
                    "\n${INDENT.repeat(2)}if let Some(it) = &$field {" +
                        "\n${attributeWriterCall("${writerCall("it", true)}?", 3)}" +
                        "\n${INDENT.repeat(2)}}"
                }
            }
        }.joinToString(separator = "\n")

        val childElements = (ancestors + listOf(element.elementNode)).map { ancestorNode ->
            Pair(ancestorNode,
                ancestorNode.children.filter { it.nodeType is NodeType.Parameter && !(it.nodeType as NodeType.Parameter).wasAttribute }
                    .map { Pair(it, it.nodeType as NodeType.Parameter) }
                    .toList()
            )
        }
            .filter { it.second.isNotEmpty() }
            .toList()

        if (isChoiceContainer(element.elementNode)) {
            element.elementNode.children[0].nodeType as NodeType.OneOf
        }


        val elementTag = dataType.originalXmlNode?.qName ?: TODO("node $dataType had no qname attached")

        val elementNamespace = when (elementTag.namespaceURI.isNotBlank()) {
            true -> """match $TAG_NAME {
${INDENT.repeat(baseIndent + 1)}Some(it) => it.0,
${INDENT.repeat(baseIndent + 1)}None => ${some("\"${elementTag.namespaceURI}\"")},
${INDENT.repeat(baseIndent)}}"""

            else -> """${INDENT.repeat(baseIndent)}match $TAG_NAME {
${INDENT.repeat(baseIndent + 1)}Some(it) => it,
${INDENT.repeat(baseIndent + 1)}None => None,
${INDENT.repeat(baseIndent)}}"""
        }

        val elementTagExpr = "$TAG_NAME.map_or_else(|| \"${elementTag.localPart}\", |(_, it)| it)"

        val startElement = """
${INDENT.repeat(baseIndent)}let element_namespace = $elementNamespace;
${INDENT.repeat(baseIndent)}let element_tag = $elementTagExpr;

${INDENT.repeat(baseIndent)}$WRITER.write_start(element_namespace, element_tag)?;
"""

        val endStartElement = """
${INDENT.repeat(baseIndent)}$WRITER.write_end(element_namespace, element_tag)?;
"""

        val mappedElements: String = when {
            element.contentWriterSimple != null -> {
                "\n${INDENT.repeat(baseIndent)}let to_write = ${callSimpleWriter("$SELF${element.contentAccess?.let { ".$it" } ?: ""}")}?;\n" +
                    "${INDENT.repeat(baseIndent)}$WRITER.write_bytes(&to_write)?;"
            }

            element.contentWriterPrimitive != null -> {
                "\n${INDENT.repeat(baseIndent)}let to_write = ${
                    callPrimitiveWriter(
                        config,
                        element.contentWriterPrimitive,
                        "&$SELF${element.contentAccess?.let { ".$it" } ?: ""}")
                }?;\n" +
                    "${INDENT.repeat(baseIndent)}$WRITER.write_bytes(&to_write)?;"
            }

            isChoiceContainer(element.elementNode) -> {

                val start = """
        match $SELF {
"""

                val end = """
        }?;
"""
                val middle: String = generateOneOfMiddle(
                    element.elementNode.children[0].children
                    , targetTypeName, primitiveWriters, simpleWriters, complexWriters)


                start + middle + end
            }

            else -> childElements.flatMap { (parent, stuff) ->
                stuff
                    .filter { !it.second.inheritance }
                    .map { (node, parameter) ->
                        val fieldName = validateName(node.nodeName.camelToSnakeCase())

                        val revAncestors = listOf(element.elementNode) + ancestors.reversed()
                        val prefix: String? = accessAncestor(revAncestors, parent)?.ifBlank { null }

                        val fieldAccess = prefix?.let { "$SELF.$it.$fieldName" } ?: "$SELF.$fieldName"
                        val rustParameter =
                            node.languageType[OutputLanguage.Rust] as RustEntry.RustParameter

                        val typeWriter = when {
                            complexWriters[parameter.parameterType.parent!!] != null -> {
                                val targetTagName: String? = node.originalXmlNode?.qName?.let { qname ->
                                    val ns = when (qname.namespaceURI.isNotBlank()) {
                                        true -> some("\"${qname.namespaceURI}\"")
                                        else -> "None"
                                    }
                                    some("($ns, \"${qname.localPart}\")")
                                }

                                when {
                                    parameter.list -> {
                                        """${INDENT.repeat(baseIndent)}for it in &$fieldAccess {
${INDENT.repeat(baseIndent + 1)}${callElementWriter("it", targetTagName)}?;
${INDENT.repeat(baseIndent)}}"""
                                    }

                                    !rustParameter.box && parameter.optional ->
                                        """${INDENT.repeat(baseIndent)}if let Some(it) = &$fieldAccess {
${INDENT.repeat(baseIndent + 1)}${callElementWriter("it", targetTagName)}?;
${INDENT.repeat(baseIndent)}};"""

                                    rustParameter.box && parameter.optional ->
                                        """${INDENT.repeat(baseIndent)}if let Some(it) = &$fieldAccess {
${INDENT.repeat(baseIndent + 1)}${callElementWriter("(*it)", targetTagName)}?;
${INDENT.repeat(baseIndent)}};"""

                                    rustParameter.box -> {
                                        "${INDENT.repeat(baseIndent)}${
                                            callElementWriter(
                                                "(*&$fieldAccess)",
                                                targetTagName
                                            )
                                        }?;"
                                    }

                                    else -> "${INDENT.repeat(baseIndent)}${
                                        callElementWriter(
                                            fieldAccess,
                                            targetTagName
                                        )
                                    }?;"
                                }
                            }

                            simpleWriters[parameter.parameterType.parent!!] != null
                                || primitiveWriters[parameter.parameterType.parent!!] != null -> {

                                val primitiveWriterOpt = primitiveWriters[parameter.parameterType.parent!!]
                                val simpleWriterOpt = simpleWriters[parameter.parameterType.parent!!]

                                val writerCall: (String) -> String = when {
                                    primitiveWriterOpt != null -> { variable ->
                                        callPrimitiveWriter(
                                            config,
                                            primitiveWriterOpt,
                                            "&$variable"
                                        )
                                    }

                                    simpleWriterOpt != null -> { variable -> callSimpleWriter(variable) }
                                    else -> TODO("Writer for ${parameter.parameterType.parent}")
                                }

                                // create custom element to represent simple type
                                fun writeFragment(
                                    fieldAccess: String,
                                    namespace: String?,
                                    elementTag: String,
                                    lvl: Int = 0
                                ) =
                                    """${INDENT.repeat(lvl)}// todo: Attributes?
${INDENT.repeat(lvl)}$WRITER.write_start(${namespace?.let { some("\"$it\"") } ?: "None"}, "$elementTag")?;
${INDENT.repeat(lvl)}let to_write = ${writerCall(fieldAccess)}?;
${INDENT.repeat(lvl)}$WRITER.write_bytes(&to_write)?
${INDENT.repeat(lvl)}    .write_end(${namespace?.let { some("\"$it\"") } ?: "None"}, "$elementTag")?"""

                                val elementTagQName = parameter.parentNode?.originalXmlNode?.qName
                                    ?: parameter.parameterType.parent?.originalXmlNode?.qName
                                    ?: TODO("node $node had no qname attached")

                                val ns = when (elementTagQName.namespaceURI.isNotBlank()) {
                                    true -> elementTagQName.namespaceURI
                                    else -> null
                                }

                                when {
                                    parameter.list -> {
                                        """${INDENT.repeat(baseIndent)}for it in &$fieldAccess {
${INDENT.repeat(baseIndent + 1)}${writeFragment("it", ns, elementTagQName.localPart, 3)};
${INDENT.repeat(baseIndent)}}"""
                                    }

                                    parameter.optional ->
                                        """${INDENT.repeat(baseIndent)}if let Some(it) = &$fieldAccess {
${writeFragment("it", ns, elementTagQName.localPart, baseIndent + 1)};
${INDENT.repeat(baseIndent)}};"""

                                    else -> "${
                                        writeFragment(
                                            fieldAccess,
                                            ns,
                                            elementTagQName.localPart,
                                            baseIndent
                                        )
                                    };"
                                }


                            }

                            else -> TODO("No writer for ${node.nodeName}: $parameter - ${parameter.parameterType}")
                        }

                        typeWriter
                    }
            }.joinToString(separator = "\n")

        }

        val elementsParser = element.elementTypeWriter?.let { generateElementTypeWriter() }


        return when (element.elementTypeWriter != null) {
            false -> functionStart + startElement + attributeParsers + xsiTypeAttr + mappedElements + endStartElement + functionEnd
            true -> functionStart + elementsParser + functionEnd
        }

    }

    private fun generateElementTypeWriter(): String {
        element.elementTypeWriter
        val field = element.elementNode.children[0].languageType[OutputLanguage.Rust] as RustEntry.RustParameter
        val fieldName = field.parameterName

        val targetTagName: String? = element.elementNode.originalXmlNode?.qName?.let { qname ->
            val ns = when (qname.namespaceURI.isNotBlank()) {
                true -> some("\"${qname.namespaceURI}\"")
                else -> "None"
            }
            some("($ns, \"${qname.localPart}\")")
        }

        return """
        ${callElementWriter("self.$fieldName", targetTagName, false)}?;
            """
    }
}

