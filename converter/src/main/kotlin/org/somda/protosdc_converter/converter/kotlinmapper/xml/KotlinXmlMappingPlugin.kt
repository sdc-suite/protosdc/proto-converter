package org.somda.protosdc_converter.converter.kotlinmapper.xml

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.mapping.base.KotlinToXmlBaseTypes
import org.somda.protosdc.mapping.base.XmlToKotlinBaseTypes
import org.somda.protosdc_converter.converter.*
import org.somda.protosdc_converter.converter.Proto3Generator.Companion.camelCaseToConstant
import org.somda.protosdc_converter.converter.kotlin.*
import org.somda.protosdc_converter.converter.kotlinmapper.FullMappingDefinition
import org.somda.protosdc_converter.converter.kotlinmapper.MapperMetadataRegistry
import org.somda.protosdc_converter.converter.kotlinmapper.mapperFunctionName
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.PrintWriter
import kotlin.reflect.KFunction
import kotlin.reflect.full.declaredFunctions

/**
 * Plugin to generate mappers XML <-> Kotlin.
 */
class KotlinXmlMappingPlugin(
    private val tree: BaseNode,
    private val protoCustomTypesTree: BaseNode,
    converterConfig: ConverterConfig,
    private val kotlinConfig: KotlinConfig,
    private val kotlinXmlMappingConfig: KotlinXmlMappingConfig,
    private val customMappers: List<XmlMapping>,
    private val generateCustomTypes: Boolean = true,
) : BaseNodePlugin, KotlinXmlMapperUtil {

    private val mergedTree = BaseNode("MergedTree", mutableListOf(tree, protoCustomTypesTree))

    private val kotlinSource = KotlinSource(
        kotlinConfig.indentationSize.toInt(),
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val kotlinMappingSource = KotlinXmlMappingSource(
        kotlinSource,
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName,
        kotlinConfig.enumTypeName
    )

    private val outputFolders = setupOutputFolders(
        converterConfig.outputFolder,
        kotlinXmlMappingConfig.outputFolders,
        kotlinXmlMappingConfig.outputFoldersNoHierarchy
    ) { "Output folders are missing for Kotlin Mapper generator" }

    override val xmlToKotlinMapperMap = MapperMetadataRegistry()
    override val kotlinToXmlMapperMap = MapperMetadataRegistry()

    /**
     * Creates a fully qualified name for a base node.
     *
     * This method throws if performed on a root node.
     */
    override fun fullyQualifiedXmlNameFor(baseNode: BaseNode): String {
        return when (val type = baseNode.nodeType) {
            is NodeType.BuiltinType -> type.origin.toString()
            is NodeType.Message -> when (val name = type.qname?.toString()) {
                null -> when (val childType = baseNode.children.first().nodeType) {
                    is NodeType.OneOf -> "${childType.qName}OneOf"
                    else -> when (baseNode.isCustomType) {
                        true -> "Custom" + baseNode.nodeName
                        else -> throw Exception("No type information available for $baseNode")
                    }
                }

                else -> name
            }

            is NodeType.OneOf -> {
                // check if this is a virtual node only by inspecting the parent for an existing XML node
                when (val xmlNode = baseNode.nodeType?.parent?.originalXmlNode) {
                    null -> "<unknown>"
                    // infer xml type mame
                    // choices do not have a type name; in order to translate choices to protobuf, we currently support
                    // - elements with nested complex type containing a choice particle
                    // - complex types containing a choice particle
                    // we do not support
                    // - extensions containing choices
                    // - nested choices of sequences or choices
                    // - and whatever might be missing here (it is XML after all...)
                    else ->
                        when (xmlNode.xmlType) {
                            is XmlType.Element -> {
                                fullyQualifiedXmlNameFor(checkNotNull(baseNode.nodeType!!.parent!!.nodeType!!.parent) {
                                    "Choice parent's parent was expected to have a node name (anonymous complex type in element)"
                                })
                            }

                            is XmlType.ComplexType -> {
                                fullyQualifiedXmlNameFor(baseNode.nodeType!!.parent!!)
                            }

                            else -> "<unknown>"
                        }
                }
            }

            is NodeType.Parameter -> fullyQualifiedXmlNameFor(type.parameterType.parent!!)
            is NodeType.Root -> "<unknown>" // no qualified name; to be used temporarily
            is NodeType.StringEnumeration -> parentForNode(baseNode)?.originalXmlNode?.qName?.toString()
                ?: throw Exception("Unknown type information for string enumeration: $baseNode")

            null -> throw Exception("Node type required, but none found for $baseNode")
        }
    }

    override fun fullyQualifiedKotlinNameFor(baseNode: BaseNode): String {
        val kotlinEntry = baseNode.kotlinLanguageType()
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parent = parentForNode(baseNode)!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        val parentIsSkip = when (parentIsRoot) {
            false -> when (val parentLanguageType = parent.kotlinLanguageType()) {
                is KotlinEntry.DataClass -> parentLanguageType.skipType
                else -> false
            }

            true -> false
        }

        when (kotlinEntry) {
            is KotlinEntry.DataClass -> {
                // if this is a node under the parent, this is correct
                if (parentIsRoot) {
                    // distinguish between builtin types (which implies "kotlin." prefix) and other types
                    return kotlinEntry.packagePath?.let {
                        kotlinEntry.importPath()
                    } ?: KotlinSource.qualifiedPathFrom(KOTLIN_PREFIX, kotlinEntry.importPath())
                }
                // resolve the parents fully qualified name and return that with the type name appended
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.typeName)
            }

            is KotlinEntry.KotlinOneOf -> {
                // if the parent is skipped, we are the fully resolved path, else we're just nested
                if (parentIsSkip) {
                    // if the OneOf is unwrapped, it has the exact same name as the parent
                    return fullyQualifiedKotlinNameFor(parent)
                }
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.oneOfName)
            }

            is KotlinEntry.KotlinOneOfParameter -> {
                // fully qualified parent is the suffix here
                val kotlinTypeNode = nodeForKotlinEntry(kotlinEntry.kotlinType)
                return fullyQualifiedKotlinNameFor(kotlinTypeNode!!)
            }

            is KotlinEntry.KotlinParameter -> {
                val kotlinTypeNode = nodeForKotlinEntry(kotlinEntry.kotlinType)
                return fullyQualifiedKotlinNameFor(kotlinTypeNode!!)
            }

            is KotlinEntry.KotlinStringEnumeration -> {
                // fully qualified parent is the suffix here
                return KotlinSource.qualifiedPathFrom(fullyQualifiedKotlinNameFor(parent), kotlinEntry.enumName)
            }
        }
    }

    override fun getKotlinFieldName(kotlin: KotlinEntry): String {
        return when (kotlin) {
            is KotlinEntry.KotlinParameter -> {
                kotlin.parameterName
            }

            else -> throw Exception("Handle $kotlin")
        }
    }

    override fun toXmlEnumName(value: String): String {
        return value.camelCaseToConstant()
    }

    private val kotlinToXmlImpl = KotlinToXmlImpl(
        kotlinSource,
        kotlinMappingSource,
        this
    )
    private val xmlToKotlinImpl = XmlToKotlinImpl(
        kotlinSource,
        kotlinMappingSource,
        this
    )

    init {
        assignMapperDataFromBuiltInMappers()
        logger.info { "Builtin mappers registered" }
        assignMapperDataFromCustomMappers()
        logger.info { "Custom mappers registered" }
    }

    override fun run() {
        logger.info("Creating Kotlin <-> XML mapping")

        assignMapperDataFromEpisodes()

        writeMapper(
            kotlinXmlMappingConfig.xmlToKotlinClassName,
            customMappers.filter { it.fromXml().code.isNotEmpty() }.map { it.fromXml() },
            ListSplit() + TextNodeCreator() + NodeListToList()
        ) { node, level, target ->
            xmlToKotlinImpl.processNode(node, target, level)
        }

        writeMapper(
            kotlinXmlMappingConfig.kotlinToXmlClassName,
            customMappers.filter { it.toXml().code.isNotEmpty() }.map { it.toXml() },
            NamespaceMapping(),
        ) { node, level, target ->
            kotlinToXmlImpl.processNode(node, target, level)
        }
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        val qualifiedXmlTypeName = fullyQualifiedXmlNameFor(node)
        val qualifiedKotlinTypeName = fullyQualifiedKotlinNameFor(node)

        // store kotlin as source and proto as target
        return EpisodeData.Mapper(
            packageName = kotlinXmlMappingConfig.mappingPackage,
            sourceToTargetFunction = kotlinToXmlMapperMap[qualifiedKotlinTypeName, qualifiedXmlTypeName]!!,
            targetToSourceFunction = xmlToKotlinMapperMap[qualifiedXmlTypeName, qualifiedKotlinTypeName]!!,
            sourceTypeName = qualifiedKotlinTypeName,
            targetTypeName = qualifiedXmlTypeName,
            sourceStructName = kotlinXmlMappingConfig.kotlinToXmlClassName,
            targetStructName = kotlinXmlMappingConfig.xmlToKotlinClassName
        )
    }

    override fun pluginIdentifier(): String {
        return KOTLIN_XML_MAPPER_PLUGIN_IDENTIFIER_V1
    }

    private fun writeMapper(
        outputClassName: String,
        customMappers: List<FullMappingDefinition>,
        builtInPreamble: List<CodeLine> = listOf(),
        processNode: (node: BaseNode, level: Int, target: PrintWriter) -> Unit
    ) {
        // determine directory structure required based on package
        val filesToWrite = determineFilesToWrite(
            outputFolders,
            kotlinXmlMappingConfig.mappingPackage,
            appendKotlinFileExtension(outputClassName)
        )

        val byteStream = ByteArrayOutputStream()
        PrintWriter(byteStream).use { printWriter ->
            // print package declaration
            printWriter.println(kotlinSource.packageDeclaration(kotlinXmlMappingConfig.mappingPackage))
            printWriter.println()

            // no imports needed, every type is rendered fully qualified

            // begin object class, all mapper functions are static
            printWriter.println(kotlinSource.objectStart(outputClassName))

            if (builtInPreamble.isNotEmpty()) {
                printWriter.println(kotlinSource.indentLines(1) {
                    builtInPreamble
                })
            }

            customMappers.forEach {
                val result = kotlinSource.indentLines(1) { it.code }
                printWriter.println(result)
            }

            tree.children.filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.skipGenerating
                    else -> true
                }
            }.filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.customType || generateCustomTypes
                    else -> true
                }
            }.forEach { node ->
                processNode(node, 1, printWriter)
            }

            printWriter.println(kotlinSource.blockEnd())
        }

        writeByteStreamToFile(byteStream, filesToWrite)
    }

    private fun assignMapperDataFromBuiltInMappers() {
        val xmlToKotlinFunctions = XmlToKotlinBaseTypes::class.declaredFunctions
        val xmlToKotlinFullyQualifiedName = XmlToKotlinBaseTypes::class.qualifiedName!!

        xmlToKotlinFunctions.forEach { func ->
            registerSignatureToKotlin(xmlToKotlinFullyQualifiedName, func, xmlToKotlinMapperMap)
        }

        val kotlinToXmlFunctions = KotlinToXmlBaseTypes::class.declaredFunctions
        val kotlinToXmlFullyQualifiedName = KotlinToXmlBaseTypes::class.qualifiedName!!
        kotlinToXmlFunctions.forEach { func ->
            registerSignatureFromKotlin(kotlinToXmlFullyQualifiedName, func, kotlinToXmlMapperMap)
        }
    }

    private fun assignMapperDataFromCustomMappers() {
        customMappers.filter { it.fromXml().code.isNotEmpty() }.forEach {
            it.fromXml().also { mapping ->
                xmlToKotlinMapperMap[mapping.sourceType, mapping.targetType] = mapping.mapperFunctionName
            }
        }
        customMappers.filter { it.toXml().code.isNotEmpty() }.forEach {
            it.toXml().also { mapping ->
                kotlinToXmlMapperMap[mapping.sourceType, mapping.targetType] = mapping.mapperFunctionName
            }
        }
    }

    private fun assignMapperDataFromEpisodes() {
        // attach all mappers stored through an episode
        tree.traversePreOrder().mapNotNull {
            it.nodeType?.asMessage()?.kotlinXmlMapperEpisodeMapperData()
        }.forEach {
            kotlinToXmlMapperMap[it.sourceTypeName, it.targetTypeName] =
                mapperFunctionName(it.packageName, it.sourceStructName, it.sourceToTargetFunction)
            xmlToKotlinMapperMap[it.targetTypeName, it.sourceTypeName] =
                mapperFunctionName(it.packageName, it.targetStructName, it.targetToSourceFunction)
        }
    }

    /**
     * Looks up the parent of a given node.
     *
     * Parent search includes the children of the main base node tree as well as the proto custom types tree,
     * which includes proto messages to express optional properties.
     */
    private fun parentForNode(node: BaseNode): BaseNode? {
        // if direct child to root, quicker result
        if (tree.children.contains(node)) {
            return tree
        }

        mergedTree.traversePreOrder().forEach { parent ->
            parent.children.forEach { child ->
                if (node === child) {
                    return parent
                }
            }
        }

        return null
    }

    /**
     * Looks up the base node for a given [KotlinEntry].
     */
    private fun nodeForKotlinEntry(kotlinEntry: KotlinEntry): BaseNode? {
        for (node in tree.traversePreOrder()) {
            if (kotlinEntry.entryId == node.kotlinLanguageTypeOrNull()?.entryId) {
                return node
            }
        }

        return null
    }

    /**
     * Looks up the base node for a given [KotlinEntry].
     */
    private fun nodeForProtoEntry(protoEntry: Proto3Generator.ProtoEntry): BaseNode? {
        return getNodeForProtoEntry(protoEntry, tree, protoCustomTypesTree)
    }

    companion object : Logging {
        private const val KOTLIN_PREFIX = "kotlin"

        fun parseConfig(config: String): KotlinXmlMappingConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, KOTLIN_XML_MAPPER_TOML_TABLE_NAME)
        }

        private fun registerSignatureToKotlin(
            fullyQualifiedName: String,
            func: KFunction<*>,
            mapperMap: MapperMetadataRegistry
        ) {
            // why can't I access the name in a sane manner somewhere? Why the is toString needed? This is stupid.
            val functionName = func.name
            val sourceType = Constants.xsdQName(functionName.substring(3).firstToLower()).toString()
            val targetType = func.returnType.toString()

            val fullyQualifiedCall = "$fullyQualifiedName.$functionName"

            mapperMap[sourceType, targetType] = fullyQualifiedCall
        }

        private fun registerSignatureFromKotlin(
            fullyQualifiedName: String,
            func: KFunction<*>,
            mapperMap: MapperMetadataRegistry
        ) {
            // why can't I access the name in a sane manner somewhere? Why the is toString needed? This is stupid.
            val functionName = func.name
            val sourceType = func.parameters[1].type.toString()
            val targetType = Constants.xsdQName(functionName.substring(3).firstToLower()).toString()

            val fullyQualifiedCall = "$fullyQualifiedName.$functionName"

            mapperMap[sourceType, targetType] = fullyQualifiedCall
        }
    }
}