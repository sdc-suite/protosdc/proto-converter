package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.xmlprocessor.*
import kotlin.math.max

const val QUALIFIED_NAME_NODE_NAME = "QualifiedName"
const val EXTENSION_NODE_NAME = "Extension"

/**
 * Enhances a [BaseNode] based tree with custom types for improved usability of the output.
 */
class CustomTypesEnricher(
    private val tree: BaseNode,
) {

    companion object : Logging

    /**
     * Creates a new [BaseNode].
     *
     * @param nodeName name of the new node
     * @param nodeType type of the new node, can be set later on
     * @param children of the new node, can be added later on
     * @param documentation of the node TODO LDe: Currently unused
     *  Only register messages, not parameters, they're not the responsible nodes for a type!
     */
    private fun createBaseNode(
        nodeName: String,
        nodeType: NodeType? = null,
        children: MutableList<BaseNode> = mutableListOf(),
        documentation: String? = null,
        originalXmlNode: XmlInfo? = null,
    ): BaseNode {
        val baseNode =
            BaseNode(nodeName, children, documentation, isCustomType = true, originalXmlNode = originalXmlNode)
        baseNode.nodeType = nodeType
        return baseNode
    }

    private fun createQNameNode(): Pair<BaseNode, NodeType.Message> {
        val stringNode = tree.findQName(BuiltinTypes.XSD_STRING.qname)!!

        val namespaceNode = createBaseNode(nodeName = "namespace")
        val namespace =
            NodeType.Parameter(parentNode = namespaceNode, parameterType = stringNode.nodeType!!, optional = false)
        namespaceNode.nodeType = namespace

        val localNameNode = createBaseNode(nodeName = "localName")
        val localName =
            NodeType.Parameter(parentNode = localNameNode, parameterType = stringNode.nodeType!!, optional = false)
        localNameNode.nodeType = localName

        val qnameNode = createBaseNode(
            QUALIFIED_NAME_NODE_NAME,
            documentation = "Qualified name with a namespace and localName, replacement for xs:QName."
        )
        // Constants.xsdQName("QName")
        val qname = NodeType.Message(qnameNode, Constants.xsdQName("QName"), customType = true)
        qnameNode.nodeType = qname
        qnameNode.children.addAll(listOf(namespaceNode, localNameNode))

        return Pair(qnameNode, qname)
    }

    private fun createExtensionNode(): Pair<BaseNode, NodeType.Message> {
        val booleanNode = tree.findQName(BuiltinTypes.XSD_BOOL.qname)!!
        val originalExtensionNode = tree.findQName(Constants.extQName("Extension"))!!
        val originalMustUnderstandNode = tree.findQName(Constants.extQName("MustUnderstand"))!!
        val mustUnderstandNode = createBaseNode(
            nodeName = "mustUnderstand",
            originalXmlNode = originalMustUnderstandNode.originalXmlNode
        )
        val mustUnderstand = NodeType.Parameter(
            parentNode = mustUnderstandNode,
            parameterType = booleanNode.nodeType!!,
            optional = false
        )
        mustUnderstandNode.nodeType = mustUnderstand

        val anyNode = tree.findQName(BuiltinTypes.XSD_ANY.qname)!!
        val extensionDataNode = createBaseNode(
            nodeName = "extensionData",
            originalXmlNode = anyNode.originalXmlNode
        )
        val extensionData =
            NodeType.Parameter(parentNode = extensionDataNode, parameterType = anyNode.nodeType!!, optional = false)
        extensionDataNode.nodeType = extensionData

        val extensionNode = createBaseNode("Item")
        val extension = NodeType.Message(extensionNode, null)
        extensionNode.nodeType = extension
        extensionNode.children.addAll(listOf(mustUnderstandNode, extensionDataNode))

        val itemNode = createBaseNode(nodeName = "item")
        val item =
            NodeType.Parameter(parentNode = extensionDataNode, parameterType = extension, optional = false, list = true)
        itemNode.nodeType = item

        val extensionsNode = createBaseNode(
            EXTENSION_NODE_NAME,
            originalXmlNode = originalExtensionNode.originalXmlNode,
            documentation = "Extensions with mustUnderstand and binary data, replacement for ext:Extension."
        )

        //
        val extensions = NodeType.Message(extensionsNode, Constants.extQName(EXTENSION_NODE_NAME), customType = true)
        extensionsNode.nodeType = extensions
        extensionsNode.children.addAll(listOf(extensionNode, itemNode))

        return Pair(extensionsNode, extensions)
    }

    /**
     * Replaces all parameters of given type with new type recursively.
     *
     * @param node to replace parameter types in
     * @param old previous type to remove
     * @param new new type to put in place of old
     */
    private fun replaceParameterType(node: BaseNode, old: BaseNode, new: NodeType.Message) {
        node.children.forEach { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Message -> {
                    replaceParameterType(childNode, old, new)
                }

                is NodeType.Parameter -> {
                    var mustReplace = nodeType.parameterType.parent == old
                    val parentMessage = node.nodeType as NodeType.Message // has to be!
                    // skip nodes which are actually extensions of our type, we don't want to replace there
                    mustReplace = mustReplace && parentMessage.extensionBaseNode != old

                    if (mustReplace) {
                        logger.info { "Replacing parameter type ${old.nodeName} in ${node.nodeName} with ${new.parentNode!!.nodeName}" }
                        nodeType.parameterType = new
                    }
                }

                else -> Unit
            }
        }
    }

    fun enrich() {
        enrichQName()
        enrichExtension()
        enrichMustUnderstand()
    }

    private fun enrichQName() {
        val (qnameNode, qnameMessage) = createQNameNode()

        // attach the qname after string builtin
        val stringNode = tree.findQName(BuiltinTypes.XSD_STRING.qname)!!

        val stringIndex = tree.children.indexOf(stringNode)

        tree.children.add(stringIndex + 1, qnameNode)

        val builtinQName = tree.findQName(BuiltinTypes.XSD_QNAME.qname)!!

        tree.children.forEach {
            replaceParameterType(it, builtinQName, qnameMessage)
        }
    }

    private fun enrichMustUnderstand() {
        tree.traversePreOrder().filter {
            when (it.nodeType as? NodeType.Parameter) {
                null -> false
                else -> when (val qName = it.originalXmlNode?.qName) {
                    null -> false
                    else -> qName == Constants.extQName("MustUnderstand")
                }
            }
        }.forEach {
            it.ignore = true
        }
    }

    private fun enrichExtension() {
        // QName declarations for removal / replacement
        val extensionElementQName = Constants.extQName("Extension")
        val extensionMustUnderstandQName = Constants.extQName("MustUnderstand")

        // create extension substitute
        val (extensionNode, newExtensionMessage) = createExtensionNode()


        // insert the extension substitute after bool and any builtin types for correct dependency placement
        val boolIndex = tree.findQName(BuiltinTypes.XSD_BOOL.qname)!!.let { tree.children.indexOf(it) }
        val anyIndex = tree.findQName(BuiltinTypes.XSD_ANY.qname)!!.let { tree.children.indexOf(it) }
        val oldExtensionMessage = tree.findQName(extensionElementQName)!!
        // remove existing extension schema definitions from tree to avoid creating additional output
        tree.children.remove(tree.findQName(extensionMustUnderstandQName)!!)
        tree.children.remove(tree.findQName(extensionElementQName)!!)

        tree.children.add(max(anyIndex, boolIndex) + 1, extensionNode)
        tree.children.filter { it.isCustomType }.forEach { logger.debug { it.nodeName } }

        // replace extension element occurrences in tree

        tree.children.forEach {
            replaceParameterType(it, oldExtensionMessage, newExtensionMessage)
        }

        logger.debug { "TEST" }


    }
}