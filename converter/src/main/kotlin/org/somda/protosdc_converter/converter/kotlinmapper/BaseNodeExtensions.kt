package org.somda.protosdc_converter.converter.kotlinmapper

import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.Constants
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.XmlType

/**
 * Returns all children that have a ext:MustUnderstand attribute.
 */
fun BaseNode.findBicepsExtensions(): List<BaseNode> {
    // get me all nodes that contain a MustUnderstand attribute
    val mustUnderstandNodes = this.children.filter(BaseNode::hasMustUnderstand)

    // distinguish between...

    // elements with mustUnderstand -> good to go
    val elements = mustUnderstandNodes.filter { it.originalXmlNode?.xmlType is XmlType.Element }

    // complexTypes with mustUnderstand -> identify enclosing elements
    val complexTypes = mustUnderstandNodes.filter { it.originalXmlNode?.xmlType is XmlType.ComplexType }

    val derivedElements = complexTypes.fold(mutableListOf<BaseNode>()) { acc, item ->
        // find nodes that have as a parameter type a complexType with mustUnderstand
        acc.apply {
            addAll(traversePreOrder()
                .filter { anyTopLevelElement ->
                    anyTopLevelElement.children.any { parameter ->
                        (parameter.nodeType as? NodeType.Parameter)?.let { paramType ->
                            (paramType.parameterType as? NodeType.Message)?.let {
                                it.qname == item.originalXmlNode!!.qName
                            }
                        } ?: false
                    }
                }
            )
        }
    }

    return elements + derivedElements
}

/**
 * Checks if a node has a ext:MustUnderstand attribute.
 */
fun BaseNode.hasMustUnderstand(): Boolean {
    val xmlType = this.originalXmlNode?.xmlType
    return if (xmlType is XmlType.Element || xmlType is XmlType.ComplexType) {
        this.children.any { baseNode ->
            baseNode.originalXmlNode?.qName?.let {
                it.namespaceURI == Constants.EXT_NAMESPACE && it.localPart == "MustUnderstand"
            } ?: false
        }
    } else {
        false
    }
}